/* eslint-disable sort-keys */
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const babelIncludes = require('./babel-includes')

const resolve = (type, entry) => {
  if (typeof entry === 'string') {
    return require.resolve(`@babel/${type}-${entry}`)
  }
  return [require.resolve(`@babel/${type}-${entry[0]}`), entry[1]]
}

const resolvePreset = entry => resolve('preset', entry)
const resolvePlugin = entry => resolve('plugin', entry)

module.exports = [
  {
    test: /\.js$|\.jsx$/,
    loader: 'babel-loader',
    query: {
      presets: [['env', { modules: false }], 'react'].map(resolvePreset),
      plugins: [
        require.resolve('react-hot-loader/babel'),
        resolvePlugin(['proposal-decorators', { legacy: true }]),
        resolvePlugin('proposal-class-properties'),
      ],
      env: {
        production: {
          /* bug requires mangle:false https://github.com/babel/minify/issues/556#issuecomment-339751209 */
          presets: [['minify', { builtIns: false, mangle: false }]],
        },
      },
    },
    include: babelIncludes,
  },
  { test: /\.png$/, loader: 'url-loader' },
  {
    test: /\.woff|\.woff2|\.svg|.eot|\.ttf/,
    loader: [
      {
        loader: 'url-loader',
        options: {
          prefix: 'font',
          limit: 1000,
        },
      },
    ],
  },
  { test: /\.html$/, loader: 'html-loader' },
  // { test: /\.json$/, loader: 'json-loader' },
  {
    test: /\.css$|\.scss$/,
    exclude: /\.local\.s?css$/, // Exclude local styles from global
    loader: [
      {
        loader: 'style-loader',
      },
      {
        loader: 'css-loader',
      },
    ],
  },
  {
    test: /\.css$|\.scss$/,
    include: /\.local\.s?css/, // Local styles
    loader: [
      {
        loader: 'style-loader',
      },
      {
        loader: MiniCssExtractPlugin.loader,
        options: {
          hmr: process.env.NODE_ENV === 'development',
        },
      },
      {
        loader: 'css-loader',
        options: {
          modules: true,
          importLoaders: 1,
          localIdentName: '[name]_[local]-[hash:base64:8]',
        },
      },
    ],
  },
]
