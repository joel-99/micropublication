# INSTALLATION

## Dependencies

The main requirements for the project are `node` and `yarn`.

If you opt to use the `Postgres` docker container for the database (see instructions below), you should also have `docker` and `docker-compose` installed.

The current release (`v0.1.0`) has been developed with `node v8.14.0`, `yarn v1.12.3`, `docker v1.13.1` and `docker-compose v1.22.0`.

You can try newer versions of these dependencies (or use `npm` instead of `yarn`), but the above setup is the one supported by the development team, as well as the one assumed for these instructions.

One final assumption is that you are running Linux or another Unix-like operating system (eg. Mac OSX, Windows Subsystem for Linux etc.).

## Setup

First, you'll need to clone the repository:

```sh
git clone https://gitlab.coko.foundation/micropubs/wormbase.git micropublication
```

### Database

#### Setup Postgres

Create an environment file:

```sh
touch config/production.env
```

Inside this file enter the following variables:

```sh
export DB_NAME='your-db-name'
export DB_PASSWORD='your-db-password'
export DB_PORT='your-db-port'
export DB_USER='your-db-user'

export NODE_ENV='production'
```

and replace the values of the first four variables with the ones you want.

The database (`Postgres`) comes in a docker container, and this container needs the environment variables outlined above. If not provided, the container will use its default values, which you can see in the `docker-compose.yml` file.

**It is highly recommended you do not use the default values, as these are public and not suitable for a production environment.**

Now you can run the database container:

```sh
. production.env && docker-compose up -d
```

This will register the environment variables and run the docker container in the background. If you do not want the container to run in the background, but to keep it open in a terminal window, simply omit the `-d` flag from the command above.

_Note: You can also use a different `Postgres` database to the one provided by the docker container (either locally running or remotely deployed). A future version will include detailed instructions on this. For the time being, you can contact us for support._

#### Create application db

Install application dependencies:

```sh
yarn
```

Create the application database in the container:

```sh
yarn setupdb
```

This will prompt you to create the `admin` user. Enter the information as requested. It will then run a script to create the required teams for the application to run. Due to a bug in the currently used `pubsweet` version, the script will not exit when it's finished. Simply killing the process (with `ctrl+c`) will do the trick.

If you ever wish to delete the database and start from scratch:

```sh
yarn resetdb
```

There is also a helper script that creates a few dummy users, in case you just want to run the app for a demo, or to check it out:

```sh
yarn seed:demo
```

Take a look at the `scripts/seedDemoData.js` file for these users' credentials. Just as above, the script will not exit by itself, so be sure to kill it.

### Application

If you haven't already done this in this terminal window / shell session, load the environment variables:

```sh
. config/production.env
```

Create an application local configuration file:

```sh
touch config/local-production.js
```

This file should look as follows:

```js
const url = 'your-url'

module.exports = {
  mailgun: {
    apiKey: 'your-mailgun-key',
    domain: 'your-mailgun-domain',
  },
  'pubsweet-client': {
    baseUrl: url,
  },
  'pubsweet-server': {
    baseUrl: url,
    db: {
      database: 'your-db-name',
      password: 'your-db-password',
      port: 'your-db-port',
      user: 'your-db-user',
    },
    host: url,
    port: 'your-application-port',
    tokenExpiresIn: '360 days',
    secret: 'your-secret',
  },
}
```

A few things to note here:

- `your-url` should be the url where the application is running. Eg. `http://my.micropublication.deployment` or `http://localhost:3000`. Do not use `https` for the time being, as the app relies on external API calls that do not support `https` (yet).
- We currently only support mailgun for sending out email. If you completely omit the `mailgun` section of the config, the app will work fine, but will not send emails.
- Make sure the database credentials under the `db` section are the same as the ones you entered in the environment file.
- We wouldn't recommend using a lower value in `tokenExpires` for the time being, until `pubsweet` supports reset tokens.

You should now be ready to run the application. You can do this with the following command:

```sh
nohup bash -c "yarn server" > /dev/null 2>&1 &
```

This will run the app in the background. Logs will be kept in the `logs` folder.

If you do not wish to run the app in the background, instead of the command above, simply run:

```sh
yarn server
```

## Development

This section makes the assumption you've already cloned the repository and met the requirements described in the beginning of this file.

For development, it is not necessary to provide an environment file, as `development` is the default `NODE_ENV` value, and it is safe to use the Postgres docker container's default provided values, as the data will not be used in production.

First run the docker container to have a running Postgres:

```sh
docker-compose up -d
```

Then follow the steps described in the **Create application db** section above.

Once you're done with that, create a local configuration file:

```sh
touch config/local-development.js
```

and export the following values:

```js
module.exports = {
  mailgun: {
    apiKey: 'your-api-key',
    domain: 'your-domain',
  },
  'pubsweet-server': {
    baseUrl: 'http://localhost:3000',
  },
}
```

Ommiting the `mailgun` section will not crash your application, but it will not send emails either.

Now you can simply run:

```sh
yarn server
```

If you wish to contribute code, make sure you read our `CONTRIBUTING` file.

## Support

Feel free to talk to us on [our chat channel](https://mattermost.coko.foundation/coko/channels/micropubs).
