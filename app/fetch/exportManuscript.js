import config from 'config'

const { baseUrl } = config['pubsweet-client']
const apiUrl = `${baseUrl}/api/export`

const exportManuscriptToHTML = articleId => {
  window.location.assign(`${apiUrl}/${articleId}/html`)
}

/* eslint-disable import/prefer-default-export */
export { exportManuscriptToHTML }
