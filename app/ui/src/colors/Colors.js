/* eslint-disable react/prop-types */

import React from 'react'
import styled, { withTheme } from 'styled-components'

const ColorWrapper = styled.div`
  align-items: center;
  display: flex;
  margin: 32px 0;
  width: 25%;
`

const Label = styled.div`
  color: ${props => props.theme.colorTextPlaceholder};
  font-family: ${props => props.theme.fontInterface};
  font-size: ${props => props.theme.fontSizeBaseSmall};
  padding-left: 16px;
`

const ColorBox = styled.div`
  background-color: ${props => props.color};
  border-radius: 10%;
  /* box-shadow: -2px 2px 14px #d6d6d6; */
  height: 50px;
  width: 50px;

  /* stylelint-disable-next-line order/properties-alphabetical-order */
  ${props =>
    (props.color === 'white' || props.color === '#FFF') &&
    `border: 2px solid gray;`}
`

const Color = props => {
  const { color, label } = props

  return (
    <ColorWrapper>
      <ColorBox color={color} />
      <Label>{label}</Label>
    </ColorWrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
`

const Colors = props => {
  const {
    colorBackground,
    colorBackgroundHue,
    colorBorder,
    colorError,
    colorSuccess,
    colorWarning,
    colorFurniture,
    colorSecondary,
    colorPrimary,
    colorText,
    colorTextPlaceholder,
    colorTextReverse,
  } = props.theme

  return (
    <Wrapper>
      <Color color={colorPrimary} label="colorPrimary" />
      <Color color={colorSecondary} label="colorSecondary" />
      <Color color={colorBackground} label="colorBackground" />
      <Color color={colorBackgroundHue} label="colorBackgroundHue" />
      <Color color={colorSuccess} label="colorSuccess" />
      <Color color={colorWarning} label="colorWarning" />
      <Color color={colorError} label="colorError" />
      <Color color={colorFurniture} label="colorFurniture" />
      <Color color={colorText} label="colorText" />
      <Color color={colorTextPlaceholder} label="colorTextPlaceholder" />
      <Color color={colorTextReverse} label="colorTextReverse" />
      <Color color={colorBorder} label="colorBorder" />
    </Wrapper>
  )
}

export default withTheme(Colors)
