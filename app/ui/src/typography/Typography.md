Interface text

```js
import faker from 'faker';
import { InterfaceText } from './Typography';

<InterfaceText>{faker.lorem.sentence()}</InterfaceText>
```

Interface text (small)

```js
import faker from 'faker';
import { InterfaceTextSmall } from './Typography';

<InterfaceTextSmall>{faker.lorem.sentence()}</InterfaceTextSmall>
```

Placeholder text

```js
import faker from 'faker';
import { PlaceholderText } from './Typography';

<PlaceholderText>{faker.lorem.sentence()}</PlaceholderText>
```

Success text

```js
import faker from 'faker';
import { SuccessText } from './Typography';

<SuccessText>{faker.lorem.sentence()}</SuccessText>
```

Warning text

```js
import faker from 'faker';
import { WarningText } from './Typography';

<WarningText>{faker.lorem.sentence()}</WarningText>
```

Error text

```js
import faker from 'faker';
import { ErrorText } from './Typography';

<ErrorText>{faker.lorem.sentence()}</ErrorText>
```

Reverse text

```js
import faker from 'faker';
import { ReverseText } from './Typography';

<ReverseText>{faker.lorem.sentence()}</ReverseText>
```

Reading text

```js
import faker from 'faker';
import { ReadingText } from './Typography';

<ReadingText>{faker.lorem.sentence()}</ReadingText>
```

Writing text

```js
import faker from 'faker';
import { WritingText } from './Typography';

<WritingText>{faker.lorem.sentence()}</WritingText>
```

Heading 1

```js
import faker from 'faker';
import { H1 } from '@pubsweet/ui';

<H1>{faker.lorem.sentence()}</H1>
```

Heading 2

```js
import faker from 'faker';
import { H2 } from '@pubsweet/ui';

<H2>{faker.lorem.sentence()}</H2>
```

Heading 3

```js
import faker from 'faker';
import { H3 } from '@pubsweet/ui';

<H3>{faker.lorem.sentence()}</H3>
```

Heading 4

```js
import faker from 'faker';
import { H4 } from '@pubsweet/ui';

<H4>{faker.lorem.sentence()}</H4>
```

Heading 5

```js
import faker from 'faker';
import { H5 } from '@pubsweet/ui';

<H5>{faker.lorem.sentence()}</H5>
```

Heading 6

```js
import faker from 'faker';
import { H6 } from '@pubsweet/ui';

<H6>{faker.lorem.sentence()}</H6>
```
