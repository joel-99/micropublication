import styled from 'styled-components'

const InterfaceText = styled.div`
  color: ${props => props.theme.colorText};
  font-family: ${props => props.theme.fontInterface};
  font-size: ${props => props.theme.fontSizeBase};
  line-height: ${props => props.theme.lineHeightBase};
  padding: 4px 8px;
`

const InterfaceTextSmall = styled.div`
  color: ${props => props.theme.colorText};
  font-family: ${props => props.theme.fontInterface};
  font-size: ${props => props.theme.fontSizeBaseSmall};
  line-height: ${props => props.theme.lineHeightBaseSmall};
  padding: 4px 8px;
`

const PlaceholderText = styled(InterfaceTextSmall)`
  color: ${props => props.theme.colorTextPlaceholder};
`

const SuccessText = styled(InterfaceText)`
  color: ${props => props.theme.colorSuccess};
`

const WarningText = styled(InterfaceText)`
  color: ${props => props.theme.colorWarning};
`

const ErrorText = styled(InterfaceText)`
  color: ${props => props.theme.colorError};
`

const ReverseText = styled(InterfaceText)`
  background-color: ${props => props.theme.colorPrimary};
  color: ${props => props.theme.colorTextReverse};
`

const ReadingText = styled(InterfaceText)`
  font-family: ${props => props.theme.fontReading};
`

const WritingText = styled(InterfaceText)`
  font-family: ${props => props.theme.fontWriting};
`

export {
  ErrorText,
  InterfaceText,
  InterfaceTextSmall,
  PlaceholderText,
  ReadingText,
  ReverseText,
  SuccessText,
  WarningText,
  WritingText,
}
