import React from 'react'
import PropTypes from 'prop-types'

import WarningModal from './WarningModal'

/**
 * General component description in JSDoc format. Markdown is *supported*.
 */
const SubmissionConfirmationModal = ({ initial, ...rest }) => (
  <WarningModal
    headerText="confirm your submission"
    textSuccess="yes"
    {...rest}
  >
    {initial &&
      `You are submitting your article to the editorial office. An editor will 
      be in touch after the initial evaluation. Are you ready to submit?`}

    {!initial &&
      `You are submitting your article, it will not be editable until after
      editorial review. Are you ready to submit?`}
  </WarningModal>
)

SubmissionConfirmationModal.defaultProps = {
  initial: true,
}

SubmissionConfirmationModal.propTypes = {
  initial: PropTypes.bool,
}

export default SubmissionConfirmationModal
