import React from 'react'
import styled from 'styled-components'

import Modal from './Modal'
import ModalHeader from './ModalHeader'
import ModalFooterDialog from './ModalFooterDialog'

// Centers info message horizontally and vertically.
const Centered = styled.div`
  align-items: center;
  display: flex;
  height: 100%;
  justify-content: center;
  text-align: center;
`

const DialogModal = props => {
  const { children, headerText, ...rest } = props

  const Header = <ModalHeader text={headerText} />
  const Footer = <ModalFooterDialog />

  return (
    <Modal
      footerComponent={Footer}
      headerComponent={Header}
      size="small"
      {...rest}
    >
      <Centered>{children}</Centered>
    </Modal>
  )
}

export default DialogModal
