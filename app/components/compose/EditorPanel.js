/* eslint-disable react/prop-types */

import React from 'react'
import { adopt } from 'react-adopt'
import { withRouter } from 'react-router-dom'
import { clone, get, last, sortBy } from 'lodash'

import {
  // queries
  getArticleForEditor,

  // mutations
  manuscriptMetadataUpdate,
  reinviteReviewer,
  // requestReviewerAttention,
  sendChatMessage,
  setSOApproval,
  submitDecision,
} from './pieces'

import { withCurrentUser } from '../../userContext'

const mapper = {
  getArticleForEditor: props => getArticleForEditor(props),
  manuscriptMetadataUpdate,
  reinviteReviewer,
  // requestReviewerAttention,
  sendChatMessage,
  setSOApproval,
  submitDecision,
}

const mapProps = args => {
  const data = get(args.getArticleForEditor, 'data.manuscript')

  let editor, scienceOfficer
  let setSOApprovalFn, submitDecisionFn, reinviteReviewerFn
  let reviewerChatThreads
  let sendSOChatMessage, sendAuthorChatMessage, sendReviewerChatMessage
  let versions

  if (data) {
    const editorTeam = data.teams.find(t => t.role === 'editor')
    editor = editorTeam.members[0] && editorTeam.members[0].user

    const scienceOfficerTeam = data.teams.find(t => t.role === 'scienceOfficer')
    scienceOfficer =
      scienceOfficerTeam.members[0] && scienceOfficerTeam.members[0].user

    const activeVersion = data.versions.find(v => v.active && v.submitted)

    if (activeVersion) {
      setSOApprovalFn = approval =>
        args.setSOApproval.setSOApproval({
          variables: {
            approval,
            manuscriptVersionId: activeVersion.id,
          },
        })

      submitDecisionFn = input =>
        args.submitDecision.submitDecision({
          variables: {
            input,
            manuscriptVersionId: activeVersion.id,
          },
        })
    }

    versions = data.versions.map(v => {
      const version = clone(v)

      const reviewerTeam = version.teams.find(t => t.role === 'reviewer')
      version.editorSuggestedReviewers = reviewerTeam.members

      const invitedReviewers = reviewerTeam.members.filter(
        m => m.status === 'invited',
      )
      const invitedCount = invitedReviewers && invitedReviewers.length

      const acceptedReviewers = reviewerTeam.members.filter(
        m => m.status === 'acceptedInvitation',
      )
      const acceptedCount = acceptedReviewers && acceptedReviewers.length

      const rejectedReviewers = reviewerTeam.members.filter(
        m => m.status === 'rejectedInvitation',
      )
      const rejectedCount = rejectedReviewers && rejectedReviewers.length

      version.reviewerCounts = {
        accepted: acceptedCount,
        invited: invitedCount + acceptedCount + rejectedCount,
        rejected: rejectedCount,
      }

      return version
    })

    versions = versions.filter(v => v.submitted)
    const latest = last(versions)
    latest.latest = true

    reinviteReviewerFn = reviewerId =>
      args.reinviteReviewer.reinviteReviewer({
        variables: {
          input: {
            manuscriptVersionId: latest.id,
            reviewerId,
          },
        },
      })

    /*
      Get all previous reviewers that have not been invited for this version
    */
    latest.previousReviewers = []
    const currentReviewerIds = latest.reviews.map(review => review.reviewer.id)
    const latestReviewerTeam = latest.teams.find(
      team => team.role === 'reviewer',
    )

    // Sort by latest first, so that when a reviewer has more than one review
    // we get the latest recommendation
    sortBy(versions, ['created'])
      .reverse()
      .forEach(v => {
        v.reviews.forEach(review => {
          const { recommendation, reviewer } = review

          if (!v.latest && !currentReviewerIds.includes(reviewer.id)) {
            const isInvited = latestReviewerTeam.members.find(
              member => member.user.id === reviewer.id,
            )

            const isCaptured = latest.previousReviewers.find(
              r => r.id === reviewer.id,
            )

            if (!isInvited && !isCaptured) {
              latest.previousReviewers.push({
                ...reviewer,
                recommendation,
              })
            }
          }
        })
      })

    const SOChat = data.chatThreads.find(
      thread => thread.chatType === 'scienceOfficer',
    )

    sendSOChatMessage = content =>
      args.sendChatMessage.sendChatMessage({
        variables: {
          input: {
            chatThreadId: SOChat.id,
            content,
          },
        },
      })

    const authorChat = data.chatThreads.find(
      thread => thread.chatType === 'author',
    )

    sendAuthorChatMessage = content =>
      args.sendChatMessage.sendChatMessage({
        variables: {
          input: {
            chatThreadId: authorChat.id,
            content,
          },
        },
      })

    reviewerChatThreads = data.chatThreads.filter(
      thread => thread.chatType === 'reviewer',
    )

    sendReviewerChatMessage = (content, chatThreadId) =>
      args.sendChatMessage.sendChatMessage({
        variables: {
          input: {
            chatThreadId,
            content,
          },
        },
      })
  }

  return {
    article: data,
    editor,
    loading: args.getArticleForEditor.loading,
    reinviteReviewer: reinviteReviewerFn,
    // requestReviewerAttention:
    //   args.requestReviewerAttention.requestReviewerAttention,
    reviewerChatThreads,
    scienceOfficer,
    sendAuthorChatMessage,
    sendReviewerChatMessage,
    sendSOChatMessage,
    setSOApproval: setSOApprovalFn,
    submitDecision: submitDecisionFn,
    updateMetadata:
      args.manuscriptMetadataUpdate.manuscriptMetadataUpdateMutation,
    versions,
  }
}

const Composed = adopt(mapper, mapProps)

const ComposedEditorPanel = props => {
  const { currentUser, match, render, ...otherProps } = props
  const { id: articleId } = match.params

  return (
    <Composed articleId={articleId} {...otherProps}>
      {mappedProps => render({ articleId, currentUser, ...mappedProps })}
    </Composed>
  )
}

// TO DO -- delete withRouter and get article id from getArticle

export default withRouter(withCurrentUser(ComposedEditorPanel))
