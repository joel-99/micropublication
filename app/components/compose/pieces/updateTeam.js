/* eslint-disable react/prop-types */

import React from 'react'
import { Mutation } from 'react-apollo'
import gql from 'graphql-tag'

import { GET_TEAMS } from './getTeams'
import { DASHBOARD_MANUSCRIPTS } from './dashboardManuscripts'
import { GET_GLOBAL_TEAMS } from './getGlobalTeams'
import { withCurrentUser } from '../../../userContext'

const UPDATE_TEAM = gql`
  mutation UpdateTeam($id: ID, $input: TeamInput!) {
    updateTeam(id: $id, input: $input) {
      id
    }
  }
`

const updateTeamMutation = props => {
  const { currentUser, render } = props

  const refetch = [
    {
      query: GET_TEAMS,
    },
    {
      query: GET_GLOBAL_TEAMS,
    },
    {
      query: DASHBOARD_MANUSCRIPTS,
      variables: { reviewerId: currentUser.id },
    },
  ]

  return (
    <Mutation mutation={UPDATE_TEAM} refetchQueries={refetch}>
      {(updateTeam, updateTeamResult) =>
        render({ updateTeam, updateTeamResult })
      }
    </Mutation>
  )
}

export { UPDATE_TEAM }

// TO DO -- withCurrentUser not needed
export default withCurrentUser(updateTeamMutation)
