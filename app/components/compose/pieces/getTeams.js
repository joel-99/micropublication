import gql from 'graphql-tag'

const GET_TEAMS = gql`
  query GetTeams {
    teams {
      global
      id
      members {
        # id
        user {
          id
          displayName
          username
        }
      }
      object {
        objectId
      }
      role
    }
  }
`

/* eslint-disable-next-line import/prefer-default-export */
export { GET_TEAMS }
