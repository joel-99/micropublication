/* eslint-disable react/prop-types */

import React from 'react'
import { Mutation } from 'react-apollo'
import gql from 'graphql-tag'

import { GET_ARTICLE_FOR_EDITOR } from './getArticleForEditor'
import { GET_USER_REVIEWS_FOR_ARTICLE } from './getUserReviewsForArticle'
import { GET_ARTICLE } from './getArticle'

const SEND_CHAT = gql`
  mutation SendChatMessage($input: SendChatMessageInput!) {
    sendChatMessage(input: $input)
  }
`

const SendChatMessageMutation = props => {
  const { articleId, render } = props

  const refetchQueries = [
    {
      query: GET_ARTICLE_FOR_EDITOR,
      variables: { id: articleId },
    },
    {
      query: GET_USER_REVIEWS_FOR_ARTICLE,
      variables: { id: articleId },
    },
    {
      query: GET_ARTICLE,
      variables: { id: articleId },
    },
  ]

  return (
    <Mutation mutation={SEND_CHAT} refetchQueries={refetchQueries}>
      {(sendChatMessage, sendChatMessageResponse) =>
        render({ sendChatMessage, sendChatMessageResponse })
      }
    </Mutation>
  )
}

export default SendChatMessageMutation
