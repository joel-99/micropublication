/* eslint-disable react/prop-types */

import React from 'react'
import { Mutation } from 'react-apollo'
import gql from 'graphql-tag'

const UPLOAD_FILE = gql`
  mutation UploadFile($file: Upload!) {
    upload(file: $file) {
      url
    }
  }
`

const UploadFileMutation = props => {
  const { render } = props

  return (
    <Mutation mutation={UPLOAD_FILE}>
      {(uploadFile, uploadFileResponse) =>
        render({ uploadFile, uploadFileResponse })
      }
    </Mutation>
  )
}

export default UploadFileMutation
