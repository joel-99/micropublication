// /* eslint-disable react/prop-types */

// import React from 'react'
// import { Mutation } from 'react-apollo'
// import gql from 'graphql-tag'

// const REQUEST_REVIEWER_ATTENTION = gql`
//   mutation RequestReviewerAttention($input: RequestReviewerAttentionInput!) {
//     requestReviewerAttention(input: $input)
//   }
// `

// const RequestReviewerAttentionMutation = props => {
//   const { render } = props

//   return (
//     <Mutation mutation={REQUEST_REVIEWER_ATTENTION}>
//       {(requestReviewerAttention, requestReviewerAttentionResponse) =>
//         render({ requestReviewerAttention, requestReviewerAttentionResponse })
//       }
//     </Mutation>
//   )
// }

// export default RequestReviewerAttentionMutation
