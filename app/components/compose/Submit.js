/* eslint-disable react/prop-types */

import React, { useState } from 'react'
import { adopt } from 'react-adopt'
import { withRouter } from 'react-router-dom'
import { get } from 'lodash'

import {
  // queries
  getArticle,

  // mutations
  saveForm,
  sendChatMessage,
  setDataType,
  submitManuscript,
  uploadFile,
} from './pieces'
import { exportManuscriptToHTML } from '../../fetch/exportManuscript'
import TabContext from '../../tabContext'
import { withCurrentUser } from '../../userContext'

const mapper = {
  getArticle: props => getArticle(props),
  saveForm,
  sendChatMessage,
  setDataType,
  submitManuscript,
  uploadFile,
}

const mapProps = args => {
  const manuscript = get(args.getArticle, 'data.manuscript')
  const manuscriptId = get(manuscript, 'id')

  const authorChat =
    manuscript &&
    manuscript.chatThreads &&
    manuscript.chatThreads.find(thread => thread.chatType === 'author')

  const authorChatMessages = authorChat && authorChat.messages

  const sendAuthorChatMessage = content =>
    args.sendChatMessage.sendChatMessage({
      variables: {
        input: {
          chatThreadId: authorChat.id,
          content,
        },
      },
    })

  const saveFormFunction = data =>
    args.saveForm.saveForm({
      variables: { input: data },
    })

  const setDataTypeFn = dataType => {
    args.setDataType.setDataType({
      variables: {
        input: {
          dataType,
        },
        manuscriptId,
      },
    })
  }

  const submit = data =>
    args.submitManuscript.submitManuscript({
      variables: { input: data },
    })

  const uploadFunction = file =>
    args.uploadFile.uploadFile({ variables: { file } })

  return {
    article: manuscript,
    authorChatMessages,
    exportManuscript: exportManuscriptToHTML,
    loading: args.getArticle.loading,
    saveForm: saveFormFunction,
    sendAuthorChatMessage,
    setDataType: setDataTypeFn,
    submit,
    upload: uploadFunction,
  }
}

const Composed = adopt(mapper, mapProps)

const ComposedSubmit = props => {
  const { currentUser, match, render, ...otherProps } = props
  const { id: articleId } = match.params
  const isAcceptedReviewerForManuscript = currentUser.auth.isAcceptedReviewerForManuscript.includes(
    articleId,
  )
  const { isAcceptedReviewerForVersion, isGlobal } = currentUser.auth
  const isAuthor = currentUser.auth.isAuthor.includes(articleId)

  const [activeTab, setActiveTab] = useState(null)
  const [locked, setLocked] = useState(true)

  return (
    <TabContext.Provider
      value={{
        activeTab,
        locked,
        toggleLock: () => setLocked(!locked),
        updateActiveTab: key => setActiveTab(key),
      }}
    >
      <Composed articleId={articleId} {...otherProps}>
        {mappedProps =>
          render({
            articleId,
            isAcceptedReviewerForManuscript,
            isAcceptedReviewerForVersion,
            isAuthor,
            isGlobal,
            ...mappedProps,
          })
        }
      </Composed>
    </TabContext.Provider>
  )
}

export default withCurrentUser(withRouter(ComposedSubmit))
