/* eslint-disable react/prop-types */

import React from 'react'
import { adopt } from 'react-adopt'
import { withRouter } from 'react-router-dom'
import { get, last } from 'lodash'

import { withCurrentUser } from '../../userContext'

import {
  // queries
  getUserReviewsForArticle,

  // mutations
  saveReview,
  sendChatMessage,
  submitReview,
} from './pieces'
import { ReviewerPanel } from '../ui'

const mapper = {
  getUserReviewsForArticle,
  saveReview,
  sendChatMessage,
  submitReview,
}

/* eslint-disable-next-line arrow-body-style */
const mapProps = args => {
  const data = get(args.getUserReviewsForArticle, 'data.manuscript')
  let versions, submitReviewFn, chatThread, sendChatMessageFn
  let saveReviewFn

  if (data && data.versions) {
    versions = data.versions.filter(v => v.reviews.length > 0)
    const latestVersion = last(versions)
    const latestReview = last(latestVersion.reviews)

    if (!latestReview.submitted) {
      saveReviewFn = input =>
        args.saveReview.saveReview({
          variables: {
            input,
            reviewId: latestReview.id,
          },
        })

      submitReviewFn = input =>
        args.submitReview.submitReview({
          variables: {
            input,
            reviewId: latestReview.id,
          },
        })
    }

    /* eslint-disable-next-line prefer-destructuring */
    chatThread = data.chatThreads[0]

    sendChatMessageFn = content =>
      args.sendChatMessage.sendChatMessage({
        variables: {
          input: {
            chatThreadId: chatThread.id,
            content,
          },
        },
      })
  }

  return {
    chatThread,
    loading: args.getUserReviewsForArticle.loading,
    saveReview: saveReviewFn,
    sendChatMessage: sendChatMessageFn,
    submitReview: submitReviewFn,
    versions,
  }
}

const Composed = adopt(mapper, mapProps)

const ComposedReviewerPanel = props => {
  const { currentUser, match } = props
  const articleId = match.params.id

  return (
    <Composed articleId={articleId}>
      {mappedProps => (
        <ReviewerPanel currentUser={currentUser} {...mappedProps} />
      )}
    </Composed>
  )
}

export default withRouter(withCurrentUser(ComposedReviewerPanel))
