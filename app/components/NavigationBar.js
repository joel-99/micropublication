/* eslint-disable react/prop-types */

import React, { useContext } from 'react'
// import PropTypes from 'prop-types'
import { Query, withApollo } from 'react-apollo'
import { matchPath } from 'react-router-dom'
import styled from 'styled-components'
import gql from 'graphql-tag'

import { Action, AppBar, Icon } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import CurrentUserContext from '../userContext'
import { shouldShowAssignReviewersLink } from '../helpers/status'

const GET_MANUSCRIPT_STATUS_FOR_NAVIGATION = gql`
  query GetManuscriptStatusForNavigation($id: ID!) {
    manuscript(id: $id) {
      versions {
        id
        decision
        submitted
      }
    }
  }
`

const Section = styled.div`
  align-items: center;
  display: flex;
`

const Item = styled.span`
  align-items: center;
  display: inline-flex;
  margin: calc(${th('gridUnit')} * 3) 1rem calc(${th('gridUnit')} * 3) 0;
`

const StyledBar = styled(AppBar)`
  flex: initial;
  height: calc(${th('gridUnit')} * 9);
  min-height: unset;

  > div:first-child > span:first-child {
    background: ${th('colorPrimary')};
    height: calc(${th('gridUnit')} * 9);
    margin: 0 calc(${th('gridUnit')} * 3) 0 0;
    padding: calc(${th('gridUnit')} * 3) 1rem;

    a {
      color: ${th('colorTextReverse')};
    }
  }
`

const navLinks = (location, currentUser) => {
  const isDashboard = location.pathname.match(/dashboard/g)
  const isArticle = location.pathname.match(/article/g)
  const isReviewers = location.pathname.match(/assign-reviewers/g)
  const isTeamManager = location.pathname.match(/teams/g)

  const isAdmin = currentUser && currentUser.admin
  const isEditor = currentUser && currentUser.auth.isGlobalEditor

  const path = `/${isArticle ? 'article' : 'assign-reviewers'}/:id`
  const match = matchPath(location.pathname, { path })
  let id
  if (match) ({ id } = match.params)

  const dashboardLink = (
    <Action active={isDashboard} to="/dashboard">
      Dashboard
    </Action>
  )

  const submitLink = (
    <Action active={isArticle} to={`/article/${id}`}>
      Article
    </Action>
  )

  const reviewersLink = isEditor ? (
    <Query query={GET_MANUSCRIPT_STATUS_FOR_NAVIGATION} variables={{ id }}>
      {({ data, loading }) => {
        if (loading) return null
        const manuscript = data && data.manuscript
        const showAssignReviewersLink = shouldShowAssignReviewersLink(
          manuscript,
        )

        if (!showAssignReviewersLink) return null

        return (
          <Action active={isReviewers} to={`/assign-reviewers/${id}`}>
            Assign Reviewers
          </Action>
        )
      }}
    </Query>
  ) : null

  const teamsLink = (
    <Action active={isTeamManager} to="/teams">
      Team Manager
    </Action>
  )

  const links = [dashboardLink]

  if (isArticle || isReviewers) {
    links.push(submitLink)
    if (reviewersLink !== null) links.push(reviewersLink)
  }

  if (isAdmin) links.push(teamsLink)

  return links
}

const RightComponent = ({ user, onLogoutClick, loginLink }) => (
  <Section>
    {user && (
      <Item>
        <Action to="/profile">
          <Icon size={2}>user</Icon>
          {user.displayName}
          {user.admin ? ' (admin)' : ''}
        </Action>
      </Item>
    )}

    {user && (
      <Item>
        <Icon size={2}>power</Icon>
        <Action onClick={onLogoutClick}>Logout</Action>
      </Item>
    )}
  </Section>
)

const NavigationBar = props => {
  const { history, location } = props
  const { currentUser, setCurrentUser } = useContext(CurrentUserContext)

  let links = null
  let onLogoutClick = () => {}

  if (currentUser) {
    links = navLinks(location, currentUser)

    onLogoutClick = () => {
      setCurrentUser(null) // clear user context
      props.client.cache.reset() // clear apollo
      localStorage.removeItem('token') // clear localstorage
      history.push('/login')
    }
  }

  return (
    <StyledBar
      brand="microPublication"
      navLinkComponents={links}
      onLogoutClick={onLogoutClick}
      rightComponent={RightComponent}
      user={currentUser}
    />
  )
}

// NavigationBar.propTypes = {
//   data: PropTypes.shape({
//     currentUser: PropTypes.shape({
//       admin: PropTypes.bool,
//       username: PropTypes.string.isRequired,
//     }),
//   }).isRequired,
//   history: PropTypes.shape({
//     push: PropTypes.func.isRequired,
//   }).isRequired,
//   location: PropTypes.shape({
//     pathname: PropTypes.string.isRequired,
//   }).isRequired,
// }

export default withApollo(NavigationBar)
