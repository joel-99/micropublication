/* eslint-disable react/prop-types */

import React from 'react'
import { Field } from 'formik'
import { fadeIn, override, th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'

import {
  CenteredColumn,
  Link,
  H1,
  ErrorText,
  Icon,
  Button,
  TextField,
} from '@pubsweet/ui'

const FormContainer = styled.div`
  ${override('Login.FormContainer')};
`

const SuccessText = styled.div`
  color: ${props => props.theme.colorSuccess};
`

const Logo = styled.div`
  margin: 0 auto;
  max-width: 100px;
  width: 100%;

  /* stylelint-disable-next-line order/properties-alphabetical-order */
  ${override('Login.Logo')};
`
Logo.displayName = 'Logo'

const SuccessWrapper = styled.div`
  animation: ${fadeIn} 1s;
  font-family: ${th('fontSizeInterface')};
  margin-top: calc(${th('gridUnit')} * 25);
  text-align: center;
`

const StyledIcon = styled(Icon)`
  margin-bottom: calc(${th('gridUnit')} * 3);
`

const UsernameInput = props => (
  <TextField label="Username" {...props.field} placeholder="Username" />
)

const GivenNamesInput = props => (
  <TextField label="Given Names" {...props.field} placeholder="Given names" />
)

const SurnameInput = props => (
  <TextField label="Surname" {...props.field} placeholder="Surname" />
)

const EmailInput = props => (
  <TextField label="Email" {...props.field} placeholder="Email" type="email" />
)

const PasswordInput = props => (
  <TextField
    label="Password"
    {...props.field}
    placeholder="Password"
    type="password"
  />
)

const Signup = ({ values, handleSubmit, status, logo = null }) => {
  const success = status && status.success

  if (success)
    return (
      <SuccessWrapper>
        <StyledIcon color={th('colorSuccess')} size={8}>
          check_circle
        </StyledIcon>
        <br />
        You have successfully signed up!
        <br />
        Your account still needs to be verified.
        <br />
        Please check your email for further instructions.
      </SuccessWrapper>
    )

  return (
    <CenteredColumn small>
      {logo && (
        <Logo>
          <img alt="pubsweet-logo" src={`${logo}`} />
        </Logo>
      )}

      {!success && (
        <FormContainer>
          <H1>Sign up</H1>

          {status && status.success && (
            <SuccessText>{status.success}</SuccessText>
          )}
          {status && status.error && <ErrorText>{status.error}</ErrorText>}

          <form onSubmit={handleSubmit}>
            <Field component={UsernameInput} name="username" />
            <Field component={GivenNamesInput} name="givenNames" />
            <Field component={SurnameInput} name="surname" />
            <Field component={EmailInput} name="email" />
            <Field component={PasswordInput} name="password" />
            <Button primary type="submit">
              Sign up
            </Button>
          </form>

          <div>
            <span>Already have an account? </span>
            <Link to="/login">Login</Link>
          </div>
        </FormContainer>
      )}
    </CenteredColumn>
  )
}

export default Signup
