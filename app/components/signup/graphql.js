import gql from 'graphql-tag'

const SIGNUP_USER = gql`
  mutation Signup($input: UserInput) {
    signUp(input: $input) {
      id
      type
      username
      email
    }
  }
`

export default SIGNUP_USER
