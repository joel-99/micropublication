/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import { clone } from 'lodash'
import ReactTable from 'react-table'
import 'react-table/react-table.css'

import { Accordion, Action, Button, List } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import ComposedAssignReviewers from './compose/AssignReviewers'
import { PageHeader, Select as DefaultSelect } from './ui'
import { TextField } from './formElements'
import { AddReviewerForm, AssignReviewersForm } from './form'
import Loading from './Loading'

const Centered = styled.div`
  margin: 0 auto;
  /* max-width: 770px; */
  width: 60%;

  > div {
    margin-bottom: calc(${th('gridUnit')} * 2);
  }
`

const Invite = styled(Action)`
  line-height: unset;
`

const Table = styled(ReactTable)`
  width: 100%;

  .rt-tbody {
    text-align: center;
  }

  .rt-th {
    &:focus {
      outline: none;
    }
  }

  div.rt-noData {
    display: none;
  }
`

const ContentWrapper = styled.div`
  cursor: default;
  display: flex;
  flex-direction: column;
  margin-left: ${th('gridUnit')};

  form {
    width: 100%;

    button {
      margin-top: calc(${th('gridUnit')} * 2);
    }
  }
`

const Select = styled(DefaultSelect)`
  max-width: unset !important;
  width: 100%;
`

const Tag = styled.span`
  background-color: ${th('colorBackgroundHue')};
  font-size: ${th('fontSizeBase')};
  line-height: ${th('lineHeightBase')};
  margin: ${th('gridUnit')} 0;
  padding: calc(${th('gridUnit')} / 2);
`

const InviteSectionWrapper = styled.div`
  margin: calc(${th('gridUnit')} * 3) 0;
`

const InviteSection = props => {
  const { addExternalReviewer } = props

  return (
    <InviteSectionWrapper>
      <Action>Add a reviewer that is not in the system</Action>

      <AddReviewerForm addExternalReviewer={addExternalReviewer}>
        {formProps => {
          const { errors, values, ...rest } = formProps

          return (
            <React.Fragment>
              <TextField
                error={errors.givenNames}
                label="Given names"
                name="givenNames"
                required
                value={values.givenNames}
                {...rest}
              />

              <TextField
                error={errors.surname}
                label="Surname"
                name="surname"
                required
                value={values.surname}
                {...rest}
              />

              <TextField
                error={errors.email}
                label="Email"
                name="email"
                required
                value={values.email}
                {...rest}
              />

              <Button primary type="submit">
                OK
              </Button>
            </React.Fragment>
          )
        }}
      </AddReviewerForm>
    </InviteSectionWrapper>
  )
}

const SuggestedReviewer = props => {
  const { name } = props
  return <Tag>{name}</Tag>
}

const SuggestedReviewers = props => {
  const { data } = props
  if (!data || data.length === 0) return null
  const items = data.map(item => {
    const i = clone(item)
    i.id = i.WBId
    return i
  })

  return <List component={SuggestedReviewer} items={items} />
}

const Section = ({ children, label }) => (
  <Accordion label={label} startExpanded>
    <ContentWrapper>{children}</ContentWrapper>
  </Accordion>
)

const EmptyMessage = styled.div`
  color: ${th('colorTextPlaceholder')};
  margin: 0 auto;
`

const LinkWrapper = styled.div`
  margin-bottom: ${th('gridUnit')};
`

const isMember = (members, userId) => members.find(m => m.user.id === userId)

const ReviewerTable = props => {
  const {
    acceptedTeam,
    articleId,
    data,
    inviteReviewer,
    invitedTeam,
    rejectedTeam,
  } = props

  const modifiedData = data.map(i => {
    const reviewer = clone(i.user)
    reviewer.name = reviewer.displayName
    return reviewer
  })

  const rows = modifiedData.map(reviewer => {
    const item = clone(reviewer)
    item.status = 'Not invited'
    item.action = 'Invite'

    if (isMember(invitedTeam, item.id)) {
      item.status = 'Invited'
      item.action = 'Re-send invitation'
    }

    if (isMember(rejectedTeam, item.id)) {
      item.status = 'Rejected'
      item.action = '-'
    } else if (isMember(acceptedTeam, item.id)) {
      item.status = 'Accepted'
      item.action = '-'
    }

    return item
  })

  const sendInvitation = (aritcleId, reviewer) => {
    const { id: reviewerId } = reviewer
    inviteReviewer(reviewerId)
  }

  const columns = [
    {
      accessor: 'name',
      Header: 'Name',
    },
    {
      accessor: 'status',
      Header: 'Status',
    },
    {
      accessor: 'agreedTc',
      Header: 'Signed up',
      /* eslint-disable-next-line sort-keys */
      Cell: context => {
        const text = context.original.agreedTc ? 'Yes' : 'No'
        return <span>{text}</span>
      },
    },
    {
      accessor: 'action',
      Header: 'Action',
      /* eslint-disable-next-line sort-keys */
      Cell: context => {
        const { original, value } = context

        if (value === '-') return value
        return (
          <Invite onClick={() => sendInvitation(articleId, original)}>
            {value}
          </Invite>
        )
      },
    },
  ]

  return (
    <React.Fragment>
      {(!modifiedData || modifiedData.length === 0) && (
        <EmptyMessage>There are currently no reviewers</EmptyMessage>
      )}

      {modifiedData && modifiedData.length > 0 && (
        <Table
          className="-striped -highlight"
          columns={columns}
          data={rows}
          minRows={0}
          resizable={false}
          showPagination={false}
        />
      )}
    </React.Fragment>
  )
}

const AssignReviewers = props => {
  const {
    addExternalReviewer,
    articleId,
    inviteReviewer,
    loading,
    suggested,
    reviewersTeam,
    users,
    updateReviewerPool,
    ...otherProps
  } = props

  if (loading) return <Loading />

  let suggestedReviewers
  if (suggested && suggested.name && suggested.name.length > 0)
    suggestedReviewers = [suggested]

  const invitedTeam = reviewersTeam.members.filter(t => t.status === 'invited')

  const acceptedTeam = reviewersTeam.members.filter(
    t => t.status === 'acceptedInvitation',
  )

  const rejectedTeam = reviewersTeam.members.filter(
    t => t.status === 'rejectedInvitation',
  )

  const userOptions = users
    ? users.map(user => ({
        label: user.displayName,
        value: user.id,
      }))
    : []

  const reviewers = reviewersTeam.members

  return (
    <Centered>
      <PageHeader>Assign Reviewers</PageHeader>

      <LinkWrapper>
        <Action to={`/article/${articleId}`}>Back to Article</Action>
      </LinkWrapper>

      <Section label="Suggested Reviewer by the Author">
        <SuggestedReviewers data={suggestedReviewers} />
      </Section>

      <Section label="Assign Reviewers to Article">
        <AssignReviewersForm
          articleId={articleId}
          reviewers={reviewers}
          updateReviewerPool={updateReviewerPool}
          {...otherProps}
        >
          {formProps => {
            const { dirty, setFieldValue, values } = formProps

            const handleChange = newValue =>
              setFieldValue('reviewers', newValue)

            return (
              <React.Fragment>
                <Select
                  closeMenuOnSelect={false}
                  isMulti
                  name="reviewers"
                  onChange={handleChange}
                  options={userOptions}
                  value={values.reviewers}
                />
                <Button disabled={!dirty} primary type="submit">
                  Save
                </Button>
              </React.Fragment>
            )
          }}
        </AssignReviewersForm>

        <InviteSection addExternalReviewer={addExternalReviewer} />
      </Section>

      <Section label="Status">
        <ReviewerTable
          acceptedTeam={acceptedTeam}
          articleId={articleId}
          data={reviewers}
          invitedTeam={invitedTeam}
          inviteReviewer={inviteReviewer}
          rejectedTeam={rejectedTeam}
        />
      </Section>
    </Centered>
  )
}

const Composed = () => <ComposedAssignReviewers render={AssignReviewers} />
export default Composed
