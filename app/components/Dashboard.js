/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import { State } from 'react-powerplug'

import { Action as UIAction } from '@pubsweet/ui'
import {
  ArticlePreviewModal,
  AuthorSectionItem,
  EditorSectionItem,
  ReviewerSectionItem,
  Section,
} from './ui'

import ComposedDashboard from './compose/Dashboard'
import Loading from './Loading'
import { DASHBOARD_MANUSCRIPTS } from './compose/pieces/dashboardManuscripts'
import { CURRENT_USER } from './Private'

const SubmitButton = props => {
  const { client, createManuscript, currentUser, history } = props

  const onClick = () => {
    createManuscript().then(res => {
      const manuscriptId = res.data.createManuscript

      /* 
        TO DO -- This needs to go. See comment in mutation.
      */
      client
        .query({
          fetchPolicy: 'network-only',
          query: CURRENT_USER,
        })
        .then(() => {
          client.query({
            fetchPolicy: 'network-only',
            query: DASHBOARD_MANUSCRIPTS,
            variables: { reviewerId: currentUser.id },
          })

          history.push(`/article/${manuscriptId}`)
        })
    })
  }

  return (
    <Action data-test-id="new-submission-button" onClick={onClick}>
      New Submission
    </Action>
  )
}

const Action = styled(UIAction)`
  line-height: unset;
`

const DashboardWrapper = styled.div`
  margin: 0 auto;
  max-width: 1024px;
`

const Dashboard = props => {
  const {
    allEditors,
    allScienceOfficers,
    authorArticles,
    client,
    createManuscript,
    currentUser,
    deleteArticle,
    editorArticles,
    handleInvitation,
    history,
    loading,
    reviewerArticles,
    scienceOfficerArticles,
    updateAssignedEditor,
    updateAssignedScienceOfficer,
  } = props

  if (loading) return <Loading />

  const isEditor = currentUser.auth.isGlobalEditor
  const isScienceOfficer = currentUser.auth.isGlobalScienceOfficer

  const headerActions = [
    <SubmitButton
      client={client}
      createManuscript={createManuscript}
      currentUser={currentUser}
      history={history}
      key="createManuscript"
    />,
  ]

  return (
    <State initial={{ previewData: null, showModal: false }}>
      {({ state, setState }) => {
        const { previewData, showModal } = state

        const openModal = article =>
          setState({
            previewData: article,
            showModal: true,
          })

        const closeModal = () =>
          setState({
            previewData: null,
            showModal: false,
          })

        const openReviewerPreviewModal = articleId => {
          const article = reviewerArticles.find(a => a.id === articleId)
          openModal(article)
        }

        return (
          <React.Fragment>
            <DashboardWrapper>
              <Section
                actions={headerActions}
                deleteArticle={deleteArticle}
                itemComponent={AuthorSectionItem}
                items={authorArticles}
                label="My Articles"
              />

              <Section
                handleInvitation={handleInvitation}
                itemComponent={ReviewerSectionItem}
                items={reviewerArticles}
                label="Reviews"
                openReviewerPreviewModal={openReviewerPreviewModal}
              />

              {isEditor && (
                <Section
                  allEditors={allEditors}
                  allScienceOfficers={allScienceOfficers}
                  itemComponent={EditorSectionItem}
                  items={editorArticles}
                  label="Editor Section"
                  updateAssignedEditor={updateAssignedEditor}
                  updateAssignedScienceOfficer={updateAssignedScienceOfficer}
                  variant="editor"
                />
              )}

              {isScienceOfficer && (
                <Section
                  itemComponent={EditorSectionItem}
                  items={scienceOfficerArticles}
                  label="Science Officer Section"
                />
              )}
            </DashboardWrapper>

            <ArticlePreviewModal
              article={previewData}
              isOpen={showModal}
              onRequestClose={closeModal}
            />
          </React.Fragment>
        )
      }}
    </State>
  )
}

const Composed = props => <ComposedDashboard render={Dashboard} {...props} />
export default Composed
