/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import Dropzone from 'react-dropzone'
import { get } from 'lodash'

import { th } from '@pubsweet/ui-toolkit'

// TO DO -- extract Labels from TextField
const Label = styled.label`
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
`

const StyledDropzone = styled(Dropzone)`
  border: 1px dashed ${th('colorFurniture')};
  border-radius: 5px;
  display: flex;
  height: calc(${th('gridUnit')} * 12);
  margin-top: ${th('gridUnit')};
  width: 400px;

  p {
    align-self: center;
    color: ${th('colorTextPlaceholder')};
    flex-grow: 1;
    text-align: center;
  }
`

const Img = styled.img`
  max-width: 600px;
`

class DropArea extends React.Component {
  constructor(props) {
    super(props)

    const file = {
      name: props.values.image.name,
      preview: `/uploads/${props.values.image.url}`,
      url: props.values.image.url,
    }

    this.state = {
      file,
      uploading: false,
    }
  }

  handleDrop = fileList => {
    const file = fileList[0]
    const { name, setFieldValue, upload } = this.props

    this.setState({
      file,
      uploading: true,
    })

    upload(file)
      .then(res => {
        this.setState({
          uploading: false,
        })

        setFieldValue(name, {
          name: file.name,
          url: res.data.upload.url,
        })
      })
      .catch(e => {
        this.setState({
          uploading: false,
        })
      })
  }

  render() {
    const { readOnly } = this.props
    const { file, uploading } = this.state

    return (
      <div data-test-id={this.props['data-test-id']}>
        {uploading && <div>Uploading...</div>}
        {!readOnly && (
          <StyledDropzone
            accept="image/*"
            data-test-id={`${this.props['data-test-id']}-dropzone`}
            onDrop={this.handleDrop}
            style={{}}
          >
            <p>
              Drop an image here {file && 'to replace current'} <br /> or click
              to select
            </p>
          </StyledDropzone>
        )}
        {file && <Img alt="" src={file.preview} />}
        {file && (
          <div>
            {/* <a href={file.url}> */}
            {file.name}
            {/* </a> */}
          </div>
        )}
      </div>
    )
  }
}

const StyledDropArea = styled(DropArea)`
  div > div:first-of-type {
    border-style: solid;
    height: 200px;
    width: 400px;
  }
`

const Error = styled.span`
  color: ${th('colorError')};
  font-size: ${th('fontSizeBaseSmall')};
  padding-left: ${th('gridUnit')};
`

const Wrapper = styled.div`
  margin-bottom: calc(${th('gridUnit')} * 2);
`

const Image = props => {
  const error = get(props.errors, 'image.url')
  const touched = get(props.touched, 'image')

  return (
    <Wrapper>
      {props.label && (
        <React.Fragment>
          <Label>{`${props.label}${props.required && ' *'}`}</Label>
          {touched && <Error>{error}</Error>}
        </React.Fragment>
      )}
      <StyledDropArea {...props} />
    </Wrapper>
  )
}

export default Image
