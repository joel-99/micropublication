import styled from 'styled-components'

import { H3 } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

const Header = styled(H3)`
  border-bottom: 1px solid ${th('colorPrimary')};
  color: ${th('colorPrimary')};
  text-transform: uppercase;
`

export default Header
