/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import { Toggle } from 'react-powerplug'

import { th } from '@pubsweet/ui-toolkit'

import ChatModal from './ChatModal'
import DiscreetButton from './DiscreetButton'

const Wrapper = styled.div`
  display: flex;
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  margin-bottom: ${th('gridUnit')};
`

const Side = styled.div`
  display: flex;
  flex-direction: column;

  span {
    padding: 2px 0;
  }
`

const Role = styled.span`
  align-self: right;
  flex-basis: 25%;
  font-variant-ligatures: none;
  margin-right: ${th('gridUnit')};
  text-align: right;

  &::after {
    content: ':';
  }
`

const Name = styled.span`
  color: ${th('colorPrimary')};
`

const NotAssigned = styled.span`
  color: ${th('colorError')};
  text-transform: uppercase;
`

const Chat = styled(DiscreetButton)`
  align-self: flex-end;
  padding-bottom: 2px;
`

const getName = user => (user ? user.displayName : null)
const NotAssignedMessage = () => <NotAssigned>not assigned</NotAssigned>

// TO DO -- hide for so
const ChatWithAuthorButton = props => {
  const { messages, sendMessage } = props

  return (
    <Toggle initial={false}>
      {({ on, toggle }) => (
        <>
          <Chat onClick={toggle}>chat with author</Chat>
          <ChatModal
            headerText="Chat with author"
            isOpen={on}
            messages={messages}
            onRequestClose={toggle}
            sendMessage={sendMessage}
          />
        </>
      )}
    </Toggle>
  )
}

const PanelInfo = props => {
  const {
    author,
    authorChatMessages,
    editor,
    isScienceOfficer,
    scienceOfficer,
    sendAuthorChatMessage,
  } = props

  const items = [
    {
      label: 'Editor',
      value: getName(editor),
    },
    {
      label: 'Science Officer',
      value: getName(scienceOfficer),
    },
    {
      label: 'Author',
      value: `${author.firstName} ${author.lastName} ( ${author.email} )`,
    },
  ]

  return (
    <Wrapper>
      <Side>
        {items.map(item => (
          <Role>{item.label}</Role>
        ))}
      </Side>

      <Side>
        {items.map(item =>
          item.value ? <Name>{item.value}</Name> : <NotAssignedMessage />,
        )}
      </Side>

      {!isScienceOfficer && (
        <ChatWithAuthorButton
          messages={authorChatMessages}
          sendMessage={sendAuthorChatMessage}
        />
      )}
    </Wrapper>
  )
}

export default PanelInfo
