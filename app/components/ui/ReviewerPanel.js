/* eslint-disable react/prop-types */

import React from 'react'
import styled, { withTheme } from 'styled-components'
import { debounce, isEqual, first, last } from 'lodash'
import { Toggle } from 'react-powerplug'

import { Button, DateParser } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import TabContext from '../../tabContext'
import PageHeader from './PageHeader'
import DiscreetButton from './DiscreetButton'
import { ReviewForm } from '../form'
import { Checkbox, Radio as UIRadio, TextEditor } from '../formElements'
import Loading from '../Loading'
import Tabs from './Tabs'
import ChatModal from './ChatModal'

const makeOptions = theme => [
  {
    color: theme.colorSuccess,
    label: 'Accept',
    value: 'accept',
  },
  {
    color: theme.colorWarning,
    label: 'Revise',
    value: 'revise',
  },
  {
    color: theme.colorError,
    label: 'Reject',
    value: 'reject',
  },
]

const Editor = styled(TextEditor)`
  min-height: 50vh;
  width: 100%;
`

const Radio = styled(UIRadio)`
  width: 100%;

  label {
    width: unset;
  }
`

const StyledPageHeader = styled(PageHeader)`
  margin-bottom: 0;
  margin-top: calc(${th('gridUnit')} * 2);
`

/* eslint-disable consistent-return */
const Colorize = styled.span`
  color: ${props => {
    if (props.color === 'warning') return th('colorWarning')
    if (props.color === 'success') return th('colorSuccess')
    if (props.color === 'error') return th('colorError')
  }};
`

const Chat = styled(DiscreetButton)`
  float: right;
`

const decisionColor = decision => {
  if (decision === 'revise') return 'warning'
  if (decision === 'accept') return 'success'
  if (decision === 'reject') return 'error'
}
/* eslint-enable consistent-return */

const Message = styled.div`
  color: ${th('colorPrimary')};
  margin-bottom: ${th('gridUnit')};
  text-transform: uppercase;
`

class AutoSave extends React.Component {
  componentWillReceiveProps(nextProps, nextContext) {
    if (!isEqual(nextProps.values, this.props.values)) {
      this.save()
    }
  }

  save = debounce(() => {
    const { values } = this.props
    Promise.resolve(this.props.onSave(values))
  }, 500)

  render() {
    return null
  }
}

const Review = props => {
  const {
    chatThread,
    decision,
    options,
    review,
    saveReview,
    sendChatMessage,
    submitReview,
    theme,
  } = props

  if (!review) return null
  const { submitted } = review.status

  if (decision && !submitted)
    return <Message>You did not submit a review for this version</Message>

  return (
    <>
      {chatThread && chatThread.messages && (
        <Toggle initial={false}>
          {({ on, toggle }) => (
            <>
              <Chat onClick={toggle}>chat with the editors</Chat>
              <ChatModal
                headerText="Chat with the editors"
                isOpen={on}
                messages={chatThread.messages}
                onRequestClose={toggle}
                sendMessage={sendChatMessage}
              />
            </>
          )}
        </Toggle>
      )}

      <ReviewForm key={review.id} review={review} submitReview={submitReview}>
        {formProps => {
          const {
            errors,
            handleChange,
            setFieldTouched,
            touched,
            values,
          } = formProps

          return (
            <>
              {submitted && <Message>Submitted</Message>}
              {submitted && decision && (
                <Message>
                  editor decision:{' '}
                  <Colorize color={decisionColor(decision)}>
                    {decision}
                  </Colorize>
                </Message>
              )}

              <Editor
                bold
                error={errors.content}
                italic
                key={submitted}
                label="Review text"
                name="content"
                placeholder="Write your review"
                readOnly={submitted}
                required
                subscript
                superscript
                touched={touched}
                value={values.content}
                {...formProps}
              />

              <Radio
                error={errors.recommendation}
                inline
                label="Recommendation to the Editors"
                name="recommendation"
                options={options}
                readOnly={submitted}
                required
                theme={theme}
                touched={touched}
                value={values.recommendation}
                {...formProps}
              />

              <Checkbox
                checkBoxText="I would like to be openly acknowledged as the reviewer"
                checked={values.openAcknowledgement}
                name="openAcknowledgement"
                onChange={handleChange}
                readOnly={submitted}
                setFieldTouched={setFieldTouched}
                touched={touched}
              />

              {!submitted && (
                <Button primary type="submit">
                  Submit
                </Button>
              )}

              <AutoSave onSave={saveReview} values={values} />
            </>
          )
        }}
      </ReviewForm>
    </>
  )
}

const ReviewerPanel = props => {
  const {
    chatThread,
    currentUser,
    loading,
    saveReview,
    sendChatMessage,
    submitReview,
    theme,
    versions,
  } = props
  const { isAcceptedReviewerForVersion } = currentUser.auth

  if (loading) return <Loading />

  const options = makeOptions(theme)
  const allowedVersions =
    versions &&
    versions.filter(v => isAcceptedReviewerForVersion.includes(v.id))

  const firstVersion = first(allowedVersions)

  const tabSections = allowedVersions.map((version, index) => {
    const latestReview = last(version.reviews)

    return {
      content: (
        <Review
          chatThread={chatThread}
          decision={version.decision}
          options={options}
          review={latestReview}
          saveReview={saveReview}
          sendChatMessage={sendChatMessage}
          submitReview={submitReview}
          theme={theme}
        />
      ),
      key: version.id,
      label: (
        <DateParser
          dateFormat="MM.DD.YY HH:mm"
          timestamp={new Date(Number(version.created))}
        >
          {timestamp => (
            <span>
              {firstVersion.id === version.id
                ? `Original: ${timestamp}`
                : `Revision ${index}: ${timestamp}`}
            </span>
          )}
        </DateParser>
      ),
    }
  })

  return (
    <TabContext.Consumer>
      {({ activeTab, locked, updateActiveTab }) => (
        <div>
          <StyledPageHeader>Review</StyledPageHeader>
          <Tabs
            activeKey={activeTab || last(versions).id}
            alwaysUseActiveKeyFromProps={locked}
            onTabClick={updateActiveTab}
            sections={tabSections}
          />
        </div>
      )}
    </TabContext.Consumer>
  )
}

export default withTheme(ReviewerPanel)
