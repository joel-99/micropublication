import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'

import RecommendationDot from './RecommendationDot'

const Header = styled.div`
  font-weight: bold;
`

const Name = styled.span`
  transition: all 0.1s ease-in;
`

const Invite = styled.span`
  border-radius: 2px;
  color: ${th('colorPrimary')};
  cursor: pointer;
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  padding: calc(${th('gridUnit')} / 2) ${th('gridUnit')} 0;
  text-transform: uppercase;
  transition: background 0.1s ease-in;

  &:hover {
    background: ${th('colorBackgroundHue')};
  }
`

const LeftSide = styled.div`
  display: flex;
`

const RowWrapper = styled.div`
  align-items: center;
  border-bottom: 1px solid ${th('colorFurniture')};
  cursor: default;
  display: flex;
  justify-content: space-between;
  margin: ${th('gridUnit')} 0;
  transition: border-color 0.1s ease-in;

  &:hover {
    border-color: ${th('colorPrimary')};
  }
`

const Comp = props => {
  const { reinviteReviewer, reviewers } = props

  return (
    <div>
      <Header>Reviewers from older versions:</Header>

      {reviewers.map(reviewer => (
        <RowWrapper>
          <LeftSide>
            <RecommendationDot recommendation={reviewer.recommendation} />
            <Name>{reviewer.displayName}</Name>
          </LeftSide>

          <Invite onClick={() => reinviteReviewer(reviewer.id)}>
            reinvite
          </Invite>
        </RowWrapper>
      ))}
    </div>
  )
}

Comp.propTypes = {
  reinviteReviewer: PropTypes.func.isRequired,
  reviewers: PropTypes.arrayOf(
    PropTypes.shape({
      displayName: PropTypes.string,
      id: PropTypes.string,
    }),
  ).isRequired,
}

Comp.defaultProps = {}

export default Comp
