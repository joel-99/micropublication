/* eslint-disable react/prop-types */
import React from 'react'

import Discuss from './Discuss'
import { Modal, ModalHeader } from '../../ui/'

const ChatModal = props => {
  const { headerText, messages, sendMessage, ...rest } = props

  const Header = <ModalHeader text={headerText || 'Chat'} />

  return (
    <Modal headerComponent={Header} size="large" {...rest}>
      <Discuss data={messages} sendChat={sendMessage} />
    </Modal>
  )
}

export default ChatModal
