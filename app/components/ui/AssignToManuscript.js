/* eslint-disable react/prop-types */

import React, { Fragment } from 'react'
import styled from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'

import Select from '../ui/Select'

const Label = styled.div`
  align-self: center;
  font-variant-ligatures: none;
  margin: 0 ${th('gridUnit')};
`

const mapUserToSelect = (user, authorIds) =>
  user && {
    isDisabled: authorIds && authorIds.includes(user.id),
    label: user.displayName,
    value: user.id,
  }

const mapUsersToOptions = (users, authorIds) =>
  users && authorIds && users.map(user => mapUserToSelect(user, authorIds))

const AssignEditor = props => {
  const {
    allOptions,
    articleId,
    authorIds,
    currentlyAssigned,
    label,
    update,
  } = props

  const options = mapUsersToOptions(allOptions, authorIds)
  const value = mapUserToSelect(currentlyAssigned)

  const handleChange = newValue => {
    const input = {
      manuscriptId: articleId,
      userId: newValue.value,
    }

    update({ variables: { input } })
  }

  return (
    <Fragment>
      <Label>{label}</Label>

      <Select
        isSearchable={false}
        onChange={handleChange}
        options={options}
        value={value}
      />
    </Fragment>
  )
}

export default AssignEditor
