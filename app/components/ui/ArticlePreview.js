/* eslint-disable react/prop-types */
import React from 'react'
import styled, { css } from 'styled-components'
import {
  clone,
  filter,
  flatten,
  forIn,
  isUndefined,
  keys,
  pickBy,
  // pull,
  // sortBy,
  uniq,
  uniqueId,
} from 'lodash'
import { Toggle } from 'react-powerplug'

import config from 'config'
import { Button, H4, H6 } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { AbstractEditor } from 'xpub-edit'

import DiscreetButton from './DiscreetButton'
import ChatModal from './ChatModal'
import PageHeader from './PageHeader'
import { unCamelCase } from '../../helpers/generic'
import { isDatatypeSelected, isFullSubmissionReady } from '../../helpers/status'
import { withCurrentUser } from '../../userContext'

const stripHTML = html => {
  const tmp = document.createElement('DIV')
  tmp.innerHTML = html
  return tmp.textContent || tmp.innerText || ''
}

const isHTMLNotEmpty = html => stripHTML(html).length > 0

/*
const mapAuthorsToValues = (authors, field) => {
  if (!authors) return null
  const affiliations = field === 'affiliations'

  // replace => remove . from end of string
  const fields = authors.map(
    author =>
      (author[field] && author[field].trim().replace(/[.]$/, '')) || null,
  )

  const withoutNull = pull(fields, null)
  const deduped = uniq(withoutNull)

  const displayValues = deduped.map((f, i) => {
    if (affiliations) return f
    if (i === deduped.length - 1) return f
    if (i === deduped.length - 2) return `${f} and `
    return `${f}, `
  })

  return displayValues
}
*/

const mapAffiliations = authors => {
  if (!authors) return null
  const affiliations = authors.map(author => author.affiliations)
  const flattened = flatten(affiliations)
  return uniq(flattened)
}

const mapAuthorsAndAffiliations = authors => {
  if (!authors) return null
  const affiliations = authors.map(author => author.affiliations)
  const flattened = flatten(affiliations)
  const affUniq = uniq(flattened)
  return authors.map(author => ({
    affiliations: author.affiliations.map(
      affiliation => affUniq.indexOf(affiliation) + 1,
    ),
    correspondingAuthor: author.correspondingAuthor,
    name: `${author.firstName} ${author.lastName}`,
  }))
}

const makeMetadataDisplayValues = geneExpression => {
  if (!geneExpression) return []
  const { detectionMethod } = geneExpression
  const { detectionMethodCorrelations } = config
  const correlations = detectionMethodCorrelations[detectionMethod]

  if (isUndefined(correlations)) return []

  const makeDisplayValue = value =>
    value.name || (isUndefined(value.name) && value) || '-'

  const extracted = correlations.map((item, i) => {
    const key = correlations[i]
    const value = geneExpression[key]
    const label = unCamelCase(key)

    const displayValue = Array.isArray(value)
      ? value.map(
          (v, pos) =>
            `${makeDisplayValue(v)}${pos === v.length - 1 ? ' ,' : ''}`,
        )
      : makeDisplayValue(value)

    return { displayValue, label }
  })

  return extracted
}

const getCorresponding = authors =>
  authors.filter(author => author.correspondingAuthor)

const Wrapper = styled.div`
  font-family: ${th('fontReading')};
  margin: 0 auto;
  max-width: 1024px;
  padding: 0 calc(${th('gridUnit')} * 7) 0 calc(${th('gridUnit')} * 6);
`

const headingStyle = css`
  color: ${th('colorText')};
`

const HeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`

const ExportWrapper = styled.div`
  display: flex;
  margin-left: auto;

  button {
    align-self: center;
  }
`

const SectionHeader = styled(H6)`
  margin: 0;
  ${headingStyle};
`

const ChatButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`

const MetadataHeader = styled(H4)`
  color: ${th('colorText')};
  margin: 0;
  padding: ${th('gridUnit')} 0;
`

const Section = styled.section`
  margin-bottom: calc(${th('gridUnit')} * 3);
  text-align: justify;
`

const Affiliations = styled.div`
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  margin-top: ${th('gridUnit')};
`

const Image = styled.img`
  height: auto;
  width: 100%;
`

const Metadata = styled.div`
  background: ${th('colorBackgroundHue')};
  font-family: ${th('fontInterface')};
  padding: ${th('gridUnit')};
`

const Editor = styled(AbstractEditor)`
  border: 0;
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeBase')};
  line-height: ${th('lineHeightBase')};
  margin-bottom: ${th('gridUnit')};
  padding: 0;
`

const Title = styled(Editor)`
  font-size: ${th('fontSizeHeading4')};
  font-weight: bold;
  line-height: ${th('lineHeightHeading4')};
  ${headingStyle};
`

const CaptionEditor = styled(Editor)`
  font-size: ${th('fontSizeBaseSmall')};
  margin: ${th('gridUnit')} 0 calc(${th('gridUnit')} * 2);

  p {
    display: inline-block;
    padding-top: calc(${th('gridUnit')} / 2);
    text-align: justify;

    &:first-child {
      padding-top: 0;

      &::before {
        content: 'Figure 1:';
        font-weight: bold;
        margin-right: ${th('gridUnit')};
      }
    }
  }
`

const MetadataEditor = styled(Editor)`
  font-family: ${th('fontInterface')};
`

const ObserveExpressionSection = styled.div`
  border-top: 2px ${th('borderStyle')} ${th('colorBorder')};
  padding-top: ${th('gridUnit')};
`

const ObserveExpressionGroup = styled.div`
  border-bottom: ${th('borderWidth')} dashed ${th('colorBorder')};
  margin-bottom: ${th('gridUnit')};
`

const InlineData = ({ label, value }) => {
  let val = clone(value)
  const isArray = Array.isArray(value)
  if (isArray) val = filter(value, item => item !== '-')
  if (!val || val === '-' || (isArray && val.length === 0)) return null

  return (
    <div>
      <strong>{`${label}: `}</strong>

      {isArray &&
        val.map((v, index) => (index === val.length - 1 ? v : `${v}, `))}

      {!isArray && val}
    </div>
  )
}

const ObserveExpression = ({ data }) => {
  const rows = []
  const keysOfInterest = [
    'certainly',
    'partially',
    'possibly',
    'not',
    'during',
    'subcellularLocalization',
  ]

  forIn(data, (v, k) => {
    if (!Array.isArray(v)) return

    /* eslint-disable array-callback-return */
    v.map(item => {
      const picked = pickBy(item, (value, key) => keysOfInterest.includes(key))
      const row = keys(picked).map(key => [key, picked[key].name || '-'])
      rows.push(row)
    })
  })

  const Group = ({ row }) => (
    <ObserveExpressionGroup>
      {row.map(element => (
        <div key={element[0]}>
          {unCamelCase(element[0])}: &nbsp; <em>{element[1]}</em>
        </div>
      ))}
    </ObserveExpressionGroup>
  )

  return (
    <ObserveExpressionSection>
      <div>
        <strong>Observe Expression</strong>
      </div>

      {rows.map(r => (
        <Group row={r} />
      ))}
    </ObserveExpressionSection>
  )
}

const Preview = props => {
  const { article, isAuthor, isGlobal, livePreview } = props

  const {
    authors,
    acknowledgements,
    comments,
    dataType,
    funding,
    geneExpression,
    image,
    imageCaption,
    laboratory,
    methods,
    reagents,
    patternDescription,
    references,
    status,
    suggestedReviewer,
    title,
  } = article

  const dataTypeSelected = isDatatypeSelected(status)
  const full = isFullSubmissionReady(status)

  let detectionMethod, expressionPattern, observeExpression, species
  if (geneExpression)
    ({
      detectionMethod,
      expressionPattern,
      observeExpression,
      species,
    } = geneExpression)

  // const sortedAuthors = sortBy(authors, 'submittingAuthor')
  const authorNames = mapAuthorsAndAffiliations(authors) // mapAuthorsToValues(sortedAuthors, 'name')
  const affiliations = mapAffiliations(authors) // mapAuthorsToValues(sortedAuthors, 'affiliations')
  const lab = laboratory && laboratory.name
  const metadata = makeMetadataDisplayValues(geneExpression)
  const correspondingAuthors = getCorresponding(authors)
  const imageSource =
    image &&
    image.url &&
    (image.url.match(/^blob/) ? image.url : `/uploads/${image.url}`)

  return (
    <>
      <Section>
        <Title
          key={uniqueId()}
          onBlur={() => {}}
          onChange={() => {}}
          readonly
          value={title}
        />

        <div>
          {authorNames.map((author, i, arr) => (
            <span key={uniqueId('author-name-')}>
              {author.name} {author.correspondingAuthor && <sup>&sect;</sup>}
              <sup>{author.affiliations.sort().join(',')}</sup>
              {i < arr.length - 1 && ', '}
            </span>
          ))}
        </div>
        <Affiliations>
          {affiliations.map((item, index) => (
            <div key={uniqueId('affiliation-')}>
              <sup>{index + 1}</sup> {item}
            </div>
          ))}

          <div>
            <sup>&sect;</sup>Correspondence to:
            <span>
              {correspondingAuthors
                .map(
                  author =>
                    ` ${author.firstName} ${author.lastName} (${author.email})`,
                )
                .join('; ')}
            </span>
          </div>
        </Affiliations>
      </Section>

      {image && image.url && (
        <Section>
          <Image alt={image.name} src={imageSource} />
          <CaptionEditor
            key={uniqueId()}
            onBlur={() => {}}
            onChange={() => {}}
            readonly
            value={imageCaption}
          />
        </Section>
      )}

      {isHTMLNotEmpty(patternDescription) && (
        <Section>
          <SectionHeader>Description:</SectionHeader>
          <Editor
            key={uniqueId()}
            onBlur={() => {}}
            onChange={() => {}}
            readonly
            value={patternDescription}
          />
        </Section>
      )}

      {isHTMLNotEmpty(methods) && (
        <Section>
          <SectionHeader>Methods:</SectionHeader>
          <Editor
            key={uniqueId()}
            onBlur={() => {}}
            onChange={() => {}}
            readonly
            value={methods}
          />
        </Section>
      )}

      {isHTMLNotEmpty(reagents) && (
        <Section>
          <SectionHeader>Reagents:</SectionHeader>
          <Editor
            key={uniqueId()}
            onBlur={() => {}}
            onChange={() => {}}
            readonly
            value={reagents}
          />
        </Section>
      )}

      {isHTMLNotEmpty(references) && (
        <Section>
          <SectionHeader>References:</SectionHeader>
          {references.map(reference => (
            <Editor
              key={uniqueId()}
              onBlur={() => {}}
              onChange={() => {}}
              readonly
              value={reference.reference}
            />
          ))}
        </Section>
      )}

      {acknowledgements && (
        <Section>
          <SectionHeader>Acknowledgments:</SectionHeader>
          {acknowledgements}
        </Section>
      )}

      {funding && (
        <Section>
          <SectionHeader>Funding:</SectionHeader>
          {funding}
        </Section>
      )}

      {(isGlobal || isAuthor) && (
        <Metadata>
          <MetadataHeader>Additional Data</MetadataHeader>

          {isHTMLNotEmpty(comments) && (
            <Section>
              <SectionHeader>Author Comments</SectionHeader>
              <MetadataEditor
                key={uniqueId()}
                onBlur={() => {}}
                onChange={() => {}}
                readonly
                value={comments}
              />
            </Section>
          )}

          {lab && <InlineData label="Laboratory" value={lab} />}

          {suggestedReviewer && (
            <Section>
              <InlineData
                label="Suggested Reviewer"
                value={suggestedReviewer.name}
              />
            </Section>
          )}

          {(dataTypeSelected || (livePreview && dataType)) && (
            <Section>
              <InlineData label="Datatype" value={unCamelCase(dataType)} />
            </Section>
          )}

          {((dataTypeSelected && full) ||
            (dataTypeSelected && livePreview)) && (
            <>
              <Section>
                {species.name && (
                  <InlineData label="Species" value={species.name} />
                )}

                {expressionPattern.name && (
                  <InlineData
                    label="Expression Pattern for Gene"
                    value={expressionPattern.name}
                  />
                )}
              </Section>

              <Section>
                {detectionMethod && (
                  <InlineData
                    label="Detection Method"
                    value={unCamelCase(detectionMethod)}
                  />
                )}

                {metadata.map(item => (
                  <InlineData
                    key={item.label}
                    label={item.label}
                    value={item.displayValue}
                  />
                ))}
              </Section>

              <ObserveExpression data={observeExpression} />
            </>
          )}
        </Metadata>
      )}
    </>
  )
}

const ArticlePreview = props => {
  // TO DO -- article is the version and article id is the manuscripts id. very confusing
  const {
    article,
    articleId,
    authorChatMessages,
    currentUser,
    exportManuscript,
    livePreview,
    sendAuthorChatMessage,
    showHeader = true,
  } = props

  const { decision } = article
  const accepted = decision === 'accept'
  const { isGlobal } = currentUser.auth
  const isAuthor = currentUser.auth.isAuthor.includes(articleId)

  return (
    <Wrapper>
      {!livePreview && !accepted && showHeader && (
        <>
          <PageHeader>Article Preview</PageHeader>

          <Toggle initial={false}>
            {({ on, toggle }) => (
              <ChatButtonWrapper>
                {isAuthor && (
                  <DiscreetButton onClick={toggle}>
                    chat with the editors
                  </DiscreetButton>
                )}

                <ChatModal
                  headerText="Chat with the editors"
                  isOpen={on}
                  messages={authorChatMessages}
                  onRequestClose={toggle}
                  sendMessage={sendAuthorChatMessage}
                />
              </ChatButtonWrapper>
            )}
          </Toggle>
        </>
      )}

      {!livePreview && accepted && (
        <HeaderWrapper>
          {showHeader && <PageHeader>Article Preview</PageHeader>}

          {!isAuthor && isGlobal && (
            <ExportWrapper>
              <Button onClick={() => exportManuscript(articleId)} primary>
                Export to HTML
              </Button>
            </ExportWrapper>
          )}
        </HeaderWrapper>
      )}

      {article && (
        <Preview
          article={article}
          isAuthor={isAuthor}
          isGlobal={isGlobal}
          livePreview={livePreview}
        />
      )}
      {!article && 'No data'}
    </Wrapper>
  )
}

export default withCurrentUser(ArticlePreview)
