/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'

import DashboardItemDate from './DashboardItemDate'
import StatusItem from './StatusItem'
import SectionItem from './SectionItem'

const StatusRowWrapper = styled.div`
  display: flex;
  margin-bottom: calc(${th('gridUnit')});
`

const StatusRow = props => {
  const { displayStatus, updated } = props

  return (
    <StatusRowWrapper>
      <StatusItem label={displayStatus} />
      {updated && <DashboardItemDate label="Last updated" value={updated} />}
    </StatusRowWrapper>
  )
}

const SectionItemWithStatus = props => {
  const { actionsComponent, displayStatus, title, updated } = props

  return (
    <React.Fragment>
      <StatusRow displayStatus={displayStatus} updated={updated} />
      <SectionItem rightComponent={actionsComponent} title={title} />
    </React.Fragment>
  )
}

export default SectionItemWithStatus
