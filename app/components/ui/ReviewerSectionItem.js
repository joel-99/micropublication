/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'

import { Action, ActionGroup } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import SectionItem from './SectionItem'

const RejectNotification = styled.div`
  color: ${th('colorError')};
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  padding: 0 ${th('gridUnit')};
  text-transform: uppercase;
`

const ActionsWrapper = styled.div`
  flex-shrink: 0;
`

const ReviewerSectionItem = props => {
  const {
    handleInvitation,
    id: articleId,
    openReviewerPreviewModal,
    reviewerStatus: status,
    reviewSubmitted,
    title,
  } = props

  let actions

  const onInvitationClick = action => handleInvitation(action, articleId)

  const InvitationActions = (
    <ActionsWrapper>
      <ActionGroup>
        <Action onClick={() => openReviewerPreviewModal(articleId)}>
          Preview
        </Action>
        <Action onClick={() => onInvitationClick('accept')}>
          Accept Invitation
        </Action>
        <Action onClick={() => onInvitationClick('reject')}>
          Reject Invitation
        </Action>
      </ActionGroup>
    </ActionsWrapper>
  )

  const ReviewLink = (
    <ActionGroup>
      <Action to={`/article/${articleId}`}>
        {status === 'acceptedInvitation' && !reviewSubmitted && 'Review'}
        {reviewSubmitted && 'Submitted review'}
      </Action>
    </ActionGroup>
  )

  if (status === 'invited') actions = InvitationActions
  if (status === 'acceptedInvitation' || reviewSubmitted) actions = ReviewLink
  if (status === 'rejectedInvitation')
    actions = <RejectNotification>Invitation Rejected</RejectNotification>

  return <SectionItem rightComponent={actions} title={title} />
}

export default ReviewerSectionItem
