/* eslint-disable react/prop-types */

import React from 'react'

import Form from './Form'

const AssignReviewersForm = props => {
  const { articleId, reviewers, updateReviewerPool, ...otherProps } = props

  const initialValues = {
    reviewers: reviewers.map(member => {
      const { displayName, id } = member.user

      return {
        label: displayName,
        value: id,
      }
    }),
  }

  const handleSubmit = (values, formikBag) => {
    const reviewerIds = values.reviewers.map(item => item.value)
    updateReviewerPool({ reviewerIds }).then(() => formikBag.resetForm(values))
  }

  return (
    <Form
      enableReinitialize
      initialValues={initialValues}
      onSubmit={handleSubmit}
      {...otherProps}
    />
  )
}

export default AssignReviewersForm
