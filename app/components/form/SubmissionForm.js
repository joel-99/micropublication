/* eslint-disable react/prop-types */

import React from 'react'

import Form from './Form'

import { dataToFormValues, formValuesToData } from '../formElements/helpers'
import makeSchema from '../formElements/validations'

const SubmissionForm = props => {
  const {
    article,
    setDataType,
    showModal,
    submitManuscript,
    upload,
    ...otherProps
  } = props

  const initialValues = dataToFormValues(article)
  const validations = makeSchema(initialValues)

  const { datatypeSelected, initial } = otherProps
  const dataTypeSelectionState = initial && !datatypeSelected

  const handleSubmit = (formValues, formikBag) => {
    const submit = () => {
      const manuscriptInput = formValuesToData(formValues)

      if (dataTypeSelectionState) {
        const { dataType } = formValues
        setDataType(dataType)
      } else {
        submitManuscript(manuscriptInput).then(() => {
          showModal()
        })
      }
    }

    submit()
  }

  const isInitialValid = validations.isValidSync(initialValues)

  return (
    <Form
      // enableReinitialize // breaks autosave
      initialValues={initialValues}
      isInitialValid={isInitialValid}
      onSubmit={handleSubmit}
      validationSchema={validations}
      {...otherProps}
    />
  )
}

export default SubmissionForm
