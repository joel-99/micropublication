/* eslint-disable react/prop-types */

import React from 'react'
import * as yup from 'yup'

import { Form } from './index'

const validations = yup.object().shape({
  decision: yup.string().required('You need to make a decision'),
  decisionLetter: yup.string().required('You need to write a decision letter'),
})

const DecisionForm = props => {
  const { data, submitDecision, ...otherProps } = props
  const { decisionLetter: letter, decision: recordedDecision } = data

  const initialValues = {
    decision: recordedDecision || '',
    decisionLetter: letter || '',
  }

  const handleSubmit = (values, formikBag) => {
    const { decision, decisionLetter } = values

    submitDecision({
      decision,
      decisionLetter,
    })
  }

  return (
    <Form
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={validations}
      {...otherProps}
    />
  )
}

export default DecisionForm
