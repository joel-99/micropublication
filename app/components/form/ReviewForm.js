/* eslint-disable react/prop-types */

import React from 'react'
import * as yup from 'yup'

import Form from './Form'

const validations = yup.object().shape({
  content: yup.string().required('Cannot leave review empty'),
  openAcknowledgement: yup.boolean(),
  recommendation: yup.string().required('You need to make a recommendation'),
})

const ReviewForm = props => {
  const { review, submitReview, ...rest } = props

  // TO DO -- remove check when you start rendering form only when there is a review
  let content, openAcknowledgement, recommendation
  if (review) ({ content, openAcknowledgement, recommendation } = review)

  const initialValues = {
    content: content || '',
    openAcknowledgement: openAcknowledgement || false,
    recommendation: recommendation || '',
  }

  const handleSubmit = (values, formikBag) => {
    submitReview(values)
  }

  return (
    <Form
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={validations}
      {...rest}
    />
  )
}

export default ReviewForm
