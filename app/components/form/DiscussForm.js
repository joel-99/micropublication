/* eslint-disable react/prop-types */

import React from 'react'
import * as yup from 'yup'

import { Form } from './index'

const stripHTML = html => {
  if (html.length === 0) return ''
  const tmp = document.createElement('DIV')
  tmp.innerHTML = html
  return tmp.textContent || tmp.innerText || ''
}

const initialValues = {
  content: '',
}

const validations = yup.object().shape({
  content: yup
    .string()
    .test(
      'chat-message-not-empty',
      'Chat message is required',
      val => stripHTML(val).length > 0,
    ),
})

const DiscussForm = props => {
  const { sendChat, ...otherProps } = props

  const handleSubmit = (values, formikBag) => {
    const { content } = values
    const { resetForm, setStatus } = formikBag

    sendChat(content).then(() => {
      resetForm()
      setStatus({ hasReset: true })
    })
  }

  return (
    <Form
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={validations}
      {...otherProps}
    />
  )
}

export default DiscussForm
