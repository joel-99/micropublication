/**
 * Deletes all data from db
 */

const { transaction } = require('objection')
const logger = require('@pubsweet/logger')

const {
  ChatMessage,
  ChatThread,
  Identity,
  Manuscript,
  ManuscriptVersion,
  Review,
  Team,
  TeamMember,
  User,
} = require('@pubsweet/models')

// Order matters. Do not change arbitrarily.
const deleteAll = async () => {
  logger.info('Deleting all data from db')

  try {
    await transaction(Manuscript.knex(), async trx => {
      logger.info('Transaction started')

      await ChatMessage.query(trx).delete()
      await ChatThread.query(trx).delete()

      await Identity.query(trx).delete()
      await User.query(trx).delete()
      await TeamMember.query(trx).delete()
      await Team.query(trx).delete()

      await ManuscriptVersion.query(trx).delete()
      await Manuscript.query(trx).delete()
      await Review.query(trx).delete()
    })

    logger.info('Data successfully deleted')
  } catch (e) {
    logger.error('Something went wrong. Rolling back...')
    throw new Error(e)
  }
}

deleteAll()
