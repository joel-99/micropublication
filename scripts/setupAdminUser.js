#! /bin/node

const logger = require('@pubsweet/logger')
const { Identity, User } = require('@pubsweet/models')

const {
  ADMIN_EMAIL: email,
  ADMIN_GIVEN_NAME: givenNames,
  ADMIN_PASSWORD: password,
  ADMIN_SURNAME: surname,
  ADMIN_USERNAME: username,
} = process.env

const makeAdminUser = async () => {
  console.log('') /* eslint-disable-line no-console */
  logger.info('### CREATING ADMIN USER ###')
  logger.info('>>> Checking if admin user already exists...')

  const adminUser = await Identity.query().findOne({
    email,
  })

  if (adminUser) {
    logger.info(`>>> Admin user "${username}" already exists.`)
    return
  }

  logger.info('>>> Admin user does not exist. Creating...')

  try {
    const user = await User.query().insert({
      admin: true,
      agreedTc: true, // TO DO -- should not be implicit, remove
      givenNames,
      password,
      surname,
      username,
    })

    await Identity.query().insert({
      email,
      isConfirmed: true,
      isDefault: true,
      userId: user.id,
    })

    logger.info(`>>> Admin user "${username}" successfully created.`)
  } catch (e) {
    logger.error(e)
    process.kill(process.pid)
  }
}

makeAdminUser()
