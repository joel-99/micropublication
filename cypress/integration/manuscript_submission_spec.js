const submitGeneExpressionFromWithDetectionMethod = method => {
  /* 
    As author
    Create manuscript with gene expression datatype
    Choose method
    Submit
    Article preview should be visible
    In the dashboard status should be "submitted"

    As editor
    In the dashboard status should be "submitted"
    Article page should have editor panel visible
  */
  cy.createManuscriptWithGeneExpressionDatatype().then(res => {
    const articleId = res.body.data.updateManuscript.id

    cy.login('author')
    cy.visit('/dashboard')

    // Check status
    cy.getById(`status-item-${articleId}`).should(
      'contain',
      'Datatype Selected',
    )

    // Go to form
    cy.getById(`author-action-edit-${articleId}`).click()
    cy.url().should('contain', `/article/${articleId}`)

    cy.get('[data-test-id=textfield-species] input')
      .type('homogona')
      .get(
        '[data-test-id="textfield-species"] .react-autosuggest__suggestion--first',
      )
      .click()

    cy.get('[data-test-id=textfield-expression-pattern] input')
      .type('ex')
      .get(
        '[data-test-id="textfield-expression-pattern"] .react-autosuggest__suggestion--first',
      )
      .click()

    switch (method) {
      case 'antibody':
        cy.getById('textfield-antibody').should('not.exist')

        cy.get('[data-test-id=radio-button-detection-method] label')
          .contains('Antibody')
          .click()

        cy.getById('textfield-antibody')
          .should('exist')
          .type('an antibody')

        break

      case 'inSituHybridization':
        cy.getById('textfield-in-situ-details').should('not.exist')

        cy.get('[data-test-id=radio-button-detection-method] label')
          .contains('In-situ Hybridization')
          .click()

        cy.getById('textfield-in-situ-details')
          .should('exist')
          .type('in situ details')

        break

      case 'genomeEditing':
        cy.getById('textfield-variation').should('not.exist')

        cy.get('[data-test-id=radio-button-detection-method] label')
          .contains('Genome Editing')
          .click()

        cy.get('[data-test-id=textfield-variation] input')
          .should('exist')
          .type('var')
          .get('.react-autosuggest__suggestion--first')
          .click()
        break

      case 'existingTransgene':
        cy.getById('existing-transgene').should('not.exist')

        cy.get('[data-test-id=radio-button-detection-method] label')
          .contains('Existing Transgene')
          .click()

        cy.getById('existing-transgene')
          .should('exist')
          .get('[data-test-id=existing-transgene-item-0] input')
          .type('tra')
          .get('.react-autosuggest__suggestion--first')
          .click()

        cy.getById('existing-transgene-add-button').click()

        cy.getById('existing-transgene')
          .should('exist')
          .get('[data-test-id=existing-transgene-item-1] input')
          .type('pre')
          .get('.react-autosuggest__suggestion--first')
          .click()

        break

      case 'newTransgene':
        cy.getById('genotype')
          .should('not.exist')
          .getById('construction-details')
          .should('not.exist')
          .getById('dna-sequence')
          .should('not.exist')
          .getById('utr')
          .should('not.exist')
          .getById('reporter')
          .should('not.exist')
          .getById('backbone-vector')
          .should('not.exist')
          .getById('fusion-type')
          .should('not.exist')
          .getById('transgene-name')
          .should('not.exist')
          .getById('construct-comments')
          .should('not.exist')
          .getById('strain')
          .should('not.exist')
          .getById('coinjected')
          .should('not.exist')
          .getById('injection-concentration')
          .should('not.exist')
          .getById('integrated-by')
          .should('not.exist')

        cy.get('[data-test-id=radio-button-detection-method] label')
          .contains('New Transgene')
          .click()

        cy.getById('genotype')
          .type('type')
          .getById('construction-details')
          .type('details')
          .getById('dna-sequence')
          .get('[data-test-id=dna-sequence-item-0] input')
          .type('sequence 1')
          .getById('dna-sequence-add-button')
          .click()
          .get('[data-test-id=dna-sequence-item-1] input')
          .type('sequence 2')
          .getById('utr')
          .get('[data-test-id=utr] input')
          .type('ut')
          .get('.react-autosuggest__suggestion--first')
          .click()
          .get('[data-test-id=reporter] input')
          .type('re')
          .get('.react-autosuggest__suggestion--first')
          .click()
          .get('[data-test-id=backbone-vector] input')
          .type('ba')
          .get('.react-autosuggest__suggestion--first')
          .click()
          .get('[data-test-id=fusion-type] input')
          .type('fu')
          .get('.react-autosuggest__suggestion--first')
          .click()
          .getById('transgene-name')
          .type('name')
          .getById('construct-comments')
          .type('comments')
          .getById('strain')
          .type('some strain')
          .getById('coinjected')
          .type('coninjected indeed')
          .getById('injection-concentration')
          .type('concentrated')
          .get('[data-test-id=integrated-by] input')
          .type('in')
          .get('.react-autosuggest__suggestion--first')
          .click()

        break

      case 'rt-pcr':
        cy.get('[data-test-id=radio-button-detection-method] label')
          .contains('RT-PCR')
          .click()

        break

      default:
        break
    }

    cy.get('[data-test-id=observe-expression-certainly] input')
      .type('soma')
      .get('.react-autosuggest__suggestion--first')
      .click()

    cy.contains('button', 'Submit').click()

    // Go through modals
    cy.get('.ReactModal__Content').should('be.visible')
    cy.contains('button', 'yes').click()

    cy.contains('successful submission').should('exist')
    cy.contains('button', 'OK').click()

    cy.contains('Article Preview').should('exist')

    // Check new status
    cy.visit('/dashboard')
    cy.getById(`status-item-${articleId}`).should('contain', 'Submitted')

    // Validate from editor's POV
    cy.login('editor')
    cy.visit('/dashboard')

    cy.getById(`status-item-${articleId}`).should('contain', 'Submitted')
    cy.getById(`go-to-article-link-${articleId}`).click()

    cy.contains('Article Preview').should('exist')
    cy.contains('Editor Panel').should('exist')
  })
}

describe('New submission', () => {
  beforeEach(() => {
    cy.login('author')
  })

  it('Author creates a new submission', () => {
    cy.visit('/dashboard')

    cy.get('[data-test-id="dashboard-section"]').should('have.length', 2)
    cy.getById('dashboard-section-item-author').should('have.length', 0)

    cy.getById('new-submission-button').click()
    cy.url().should('include', '/article')
    cy.contains('Submit your article').should('exist')

    cy.contains('Dashboard').click()
    cy.url().should('include', '/dashboard')

    cy.getById('dashboard-section-item-author').should('have.length', 1)
  })

  it('Author finishes initial submission', () => {
    cy.createEmptyManuscript().then(res => {
      cy.getCurrentUser().then(res2 => {
        cy.visit(`/article/${res.body.data.createManuscript}`)
      })
    })

    cy.get('[data-test-id="authorinput-author.name"] input')
      .type('John Brown')
      .get('[data-test-id="authorinput-author"] .react-select__control')
      .click()
      .get(
        '[data-test-id="authorinput-author"] .react-select__option:nth-child(1)',
      )
      .click()
      .get(
        '[data-test-id="authorinput-author"] .react-select__option:nth-child(1)',
      )
      .click()
      .getById('authorinput-author.affiliations')
      .type('University of Chicago')
      .getById('author-email')
      .type('johnbrown@example.com')

    cy.get('[data-test-id="authorinput-coAuthors[0].name"] input')
      .type('Han')
      .get(
        '[data-test-id="authorinput-coAuthors[0]"] .react-autosuggest__suggestion--first',
      )
      .click()
      .get('[data-test-id="authorinput-coAuthors[0]"] .react-select__control')
      .click()
      .get(
        '[data-test-id="authorinput-coAuthors[0]"] .react-select__option:nth-child(3)',
      )
      .click()
      .getById('authorinput-coAuthors[0].affiliations')
      .type('University of Michigan')

    cy.get(
      '[data-test-id="coauthors"] [data-test-id="coauthors-add-button"]',
    ).click()

    cy.get('[data-test-id="authorinput-coAuthors[1].name"] input')
      .type('Jack Black')
      .get('[data-test-id="authorinput-coAuthors[1]"] .react-select__control')
      .click()
      .get(
        '[data-test-id="authorinput-coAuthors[1]"] .react-select__option:nth-child(4)',
      )
      .click()
      .getById('authorinput-coAuthors[1].affiliations')
      .type('University of Wisconsin')

    cy.get('[data-test-id="laboratory"] input')
      .type('lab')
      .get('[data-test-id="laboratory"] .react-autosuggest__suggestion--first')
      .click()

    cy.getById('funding').type('The McMoney Foundation')

    cy.get('[data-test-id="title"] [contenteditable=true]').type(
      'This is a great title',
    )

    cy.fixture('files/submission_image.png', 'base64').then(content => {
      cy.get('[data-test-id=image-dropzone]').upload(content, 'image.png')
    })

    cy.get('[data-test-id="image-caption"] [contenteditable=true]')
      // .click()
      .type('A picture is a thousand words')

    cy.get('[data-test-id="pattern-description"] [contenteditable=true]').type(
      'Lorem ipsum dolor sit amet.',
    )

    cy.get('[data-test-id="methods"] [contenteditable=true]').type(
      'Consectetur adipiscing elit.',
    )

    cy.get('[data-test-id="references"] [contenteditable=true]').type(
      'Nunc aliquam mi massa, non egestas sem bibendum sit amet.',
    )

    cy.getById('acknowledgements').type('I acknowledge everything')

    cy.get('[data-test-id="suggested-reviewer"] input').type('Jackie Brown')

    cy.get('[data-test-id="disclaimer"] span').click()

    cy.get('[data-test-id="comments"] [contenteditable=true]').type(
      'Quisque bibendum, lectus et facilisis luctus.',
    )

    cy.contains('button', 'Submit').click()

    cy.get('.ReactModal__Content').should('be.visible')
    cy.contains('button', 'yes').click()

    cy.contains('successful submission').should('exist')
    cy.contains('button', 'OK').click()

    cy.contains('Article Preview').should('exist')
  })

  it('Editor assigns datatype', () => {
    /* 
      Add two manuscripts.
      One should have datatype gene expression, the other one should have none.
      Statuses should be 'initial submission ready'.
      Going to the submission page should display the form in read-only mode
      Select a datatype
      Then you should get:
      - article view if datatype selected
      - article view with editor panel if no datatype selected
      Statuses should now be 'datatype selected' and 'submitted'
    */
    const isInitialFormReadonly = () => {
      cy.get('[data-test-id="authorinput-author.name"] input')
        .should('have.attr', 'readonly')
        .get('[data-test-id="authorinput-author"] .react-select__control')
        .should('have.class', 'react-select__control--is-disabled')
        .getById('authorinput-author.affiliations')
        .should('have.attr', 'readonly')
        .getById('author-email')
        .should('have.attr', 'readonly')

      cy.get('[data-test-id="authorinput-coAuthors[0].name"] input')
        .should('have.attr', 'readonly')
        .get('[data-test-id="authorinput-coAuthors[0]"] .react-select__control')
        .should('have.class', 'react-select__control--is-disabled')
        .getById('authorinput-coAuthors[0].affiliations')
        .should('have.attr', 'readonly')

      cy.get('[data-test-id="laboratory"] input').should(
        'have.attr',
        'readonly',
      )

      cy.getById('funding').should('have.attr', 'readonly')

      cy.get('[data-test-id="title"] .ProseMirror').should(
        'have.attr',
        'contenteditable',
        'false',
      )

      cy.getById('image').should('exist')
      cy.getById('image-dropzone').should('not.exist')

      cy.get('[data-test-id="image-caption"] .ProseMirror').should(
        'have.attr',
        'contenteditable',
        'false',
      )

      cy.get('[data-test-id="pattern-description"] .ProseMirror').should(
        'have.attr',
        'contenteditable',
        'false',
      )

      cy.get('[data-test-id="methods"] .ProseMirror').should(
        'have.attr',
        'contenteditable',
        'false',
      )

      cy.get('[data-test-id="references"] .ProseMirror').should(
        'have.attr',
        'contenteditable',
        'false',
      )

      cy.getById('acknowledgements').should('have.attr', 'readonly')

      cy.get('[data-test-id="suggested-reviewer"] input').should(
        'have.attr',
        'readonly',
      )

      cy.get('[data-test-id="disclaimer"] input').should(
        'have.attr',
        'disabled',
      )

      cy.get('[data-test-id="comments"] .ProseMirror').should(
        'have.attr',
        'contenteditable',
        'false',
      )
    }

    // Add two manuscripts and get their ids
    cy.createInitialSubmission().then(res => {
      const articleIds = res.map(item => item.body.data.updateManuscript.id)

      const ids = {
        withDatatype: articleIds[0],
        withoutDatatype: articleIds[1],
      }

      cy.login('editor')
      cy.visit('/dashboard')

      cy.getById('dashboard-section').should('have.length', 3)

      // Check initial statuses
      cy.getById(`status-item-${ids.withDatatype}`).should(
        'contain',
        'Initial submission ready',
      )
      cy.getById(`status-item-${ids.withoutDatatype}`).should(
        'contain',
        'Initial submission ready',
      )

      // Go to fist one and choose "gene expression"
      cy.getById(`go-to-article-link-${ids.withDatatype}`).click()

      const urlWithDatatype = `/article/${ids.withDatatype}`
      cy.url().should('include', urlWithDatatype)

      isInitialFormReadonly()

      cy.get('[data-test-id=datatype-select] .react-select__control').click()

      cy.contains(
        '[data-test-id=datatype-select] .react-select__option',
        'Gene expression results',
      ).click()

      cy.contains('button', 'Submit').click()

      cy.contains('Article Preview').should('exist')
      cy.contains('Editor Panel').should('not.exist')

      // Go to dashboard and check statuses
      cy.visit('/dashboard')

      cy.getById(`status-item-${ids.withDatatype}`).should(
        'contain',
        'Datatype Selected',
      )

      cy.getById(`status-item-${ids.withoutDatatype}`).should(
        'contain',
        'Initial submission ready',
      )

      // Go to second one and choose "no datatype"
      cy.getById(`go-to-article-link-${ids.withoutDatatype}`).click()

      const urlWithoutDatatype = `/article/${ids.withoutDatatype}`
      cy.url().should('include', urlWithoutDatatype)

      isInitialFormReadonly()

      cy.get('[data-test-id=datatype-select] .react-select__control').click()

      cy.contains(
        '[data-test-id=datatype-select] .react-select__option',
        'No Datatype',
      ).click()

      cy.contains('button', 'Submit').click()

      cy.contains('Article Preview').should('exist')
      cy.contains('Editor Panel').should('exist')

      // Check final statuses
      cy.visit('/dashboard')

      cy.getById(`status-item-${ids.withDatatype}`).should(
        'contain',
        'Datatype Selected',
      )

      cy.getById(`status-item-${ids.withoutDatatype}`).should(
        'contain',
        'Submitted',
      )
    })
  })

  it('Author submits full manuscript: Gene expression - Antibody', () => {
    submitGeneExpressionFromWithDetectionMethod('antibody')
  })

  it('Author submits full manuscript: Gene expression - In Situ Hybridization', () => {
    submitGeneExpressionFromWithDetectionMethod('inSituHybridization')
  })

  it('Author submits full manuscript: Gene expression - Genome Editing', () => {
    submitGeneExpressionFromWithDetectionMethod('genomeEditing')
  })

  it('Author submits full manuscript: Gene expression - Existing transgene', () => {
    submitGeneExpressionFromWithDetectionMethod('existingTransgene')
  })

  it('Author submits full manuscript: Gene expression - New transgene', () => {
    submitGeneExpressionFromWithDetectionMethod('newTransgene')
  })

  it('Author submits full manuscript: Gene expression - RT-PCR', () => {
    submitGeneExpressionFromWithDetectionMethod('rt-pcr')
  })
})
