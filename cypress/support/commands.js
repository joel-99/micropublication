// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

/* eslint-disable sort-keys */

import { v4 as uuid } from 'uuid'
import { clone } from 'lodash'

const credentials = require('./credentials')

const manuscriptData = {
  acknowledgements: 'Some acknowledgements',
  authors: [
    {
      affiliations: 'University of Chicago',
      credit: ['software'],
      email: 'someone@example.com',
      name: 'John Green',
      submittingAuthor: true,
      WBId: 'WB1999',
    },
  ],
  comments: 'These are the comments',
  disclaimer: true,
  funding: 'Some funding',
  geneExpression: {
    antibodyUsed: '',
    backboneVector: { name: '', WBId: '' },
    coinjected: '',
    constructComments: '',
    constructionDetails: '',
    detectionMethod: '',
    dnaSequence: [{ name: '', WBId: '' }],
    expressionPattern: { name: '', WBId: '' },
    fusionType: { name: '', WBId: '' },
    genotype: '',
    injectionConcentration: '',
    inSituDetails: '',
    integratedBy: { name: '', WBId: '' },
    observeExpression: {
      certainly: [
        {
          certainly: {
            // id: uuid(),
            name: '',
            WBId: '',
          },
          during: {
            // id: uuid(),
            name: '',
            WBId: '',
          },
          id: uuid(),
          subcellularLocalization: {
            // id: uuid(),
            name: '',
            WBId: '',
          },
        },
      ],
      not: [
        {
          during: {
            // id: uuid(),
            name: '',
            WBId: '',
          },
          id: uuid(),
          not: {
            // id: uuid(),
            name: '',
            WBId: '',
          },
          subcellularLocalization: {
            // id: uuid(),
            name: '',
            WBId: '',
          },
        },
      ],
      partially: [
        {
          during: {
            // id: uuid(),
            name: '',
            WBId: '',
          },
          id: uuid(),
          partially: {
            // id: uuid(),
            name: '',
            WBId: '',
          },
          subcellularLocalization: {
            // id: uuid(),
            name: '',
            WBId: '',
          },
        },
      ],
      possibly: [
        {
          during: {
            // id: uuid(),
            name: '',
            WBId: '',
          },
          id: uuid(),
          possibly: {
            // id: uuid(),
            name: '',
            WBId: '',
          },
          subcellularLocalization: {
            // id: uuid(),
            name: '',
            WBId: '',
          },
        },
      ],
    },
    reporter: { name: '', WBId: '' },
    species: { name: '', WBId: '' },
    strain: '',
    transgeneName: '',
    transgeneUsed: [{ name: '', WBId: '' }],
    utr: { name: '', WBId: '' },
    variation: { name: '', WBId: '' },
  },
  imageCaption: '<p>This is the image caption</p>',
  laboratory: {
    name: 'The lab',
    WBId: 'WB9111',
  },
  methods: '<p>These are the methods</p>',
  patternDescription: '<p>This is the pattern description</p>',
  references: '<p>These are the references</p>',
  status: {
    decision: {
      accepted: false,
      rejected: false,
      revise: false,
    },
    scienceOfficer: {
      approved: null,
      pending: false,
    },
    submission: {
      initial: true,
      datatypeSelected: false,
      full: false,
    },
  },
  suggestedReviewer: {
    name: 'Jane Blue',
    WBId: 'WB1921',
  },
  title: '<p>This is the title</p>',
}

const submitManuscriptMutation = `
  mutation submitManuscript($data: ManuscriptInput!) {
    updateManuscript(data: $data) {
      id
    }
}`

const upload = `
  mutation uploadFile($file: Upload!) {
    upload(file: $file) {
      url
    }
  }
`

Cypress.Commands.add('login', role => {
  const { username, password } = credentials[role]

  const loginUserMutation = `mutation{
    loginUser (input: {
      username: "${username}",
      password: "${password}"
  }) {
      token
    }
  }`

  window.localStorage.removeItem('token')

  cy.request({
    method: 'POST',
    url: '/graphql',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: { query: loginUserMutation },
  }).then(response => {
    const { token } = response.body.data.loginUser
    window.localStorage.setItem('token', token)
  })
})

Cypress.Commands.add('getById', id => cy.get(`[data-test-id="${id}"]`))

// Custom comand to handle uploading a file using react-dropzone
// Adapted from https://gist.github.com/ZwaarContrast/00101934954980bcaa4ae70ac9930c60
Cypress.Commands.add(
  'upload',
  { prevSubject: 'element' },
  (subject, file, fileName) => {
    // we need to access window in order to create a file below
    cy.window().then(window => {
      const contentType = 'image/png'

      Cypress.Blob.base64StringToBlob(file, contentType).then(blob => {
        const testFile = new window.File([blob], fileName, {
          type: contentType,
        })

        cy.wrap(subject).trigger('drop', {
          dataTransfer: { files: [testFile], types: ['Files'] },
        })
      })
    })
  },
)

const uploadProgrammatically = (fixture, name) =>
  cy.fixture(fixture, 'base64').then(content => {
    Cypress.Blob.base64StringToBlob(content, 'image/png').then(async blob => {
      const imageFile = new File([blob], name, {
        type: 'image/png',
      })

      const body = new FormData()

      body.append(
        'operations',
        JSON.stringify({
          operationName: 'uploadFile',
          variables: {
            file: null,
          },
          query: upload,
        }),
      )

      body.append('map', JSON.stringify({ 1: ['variables.file'] }))
      body.append('1', imageFile, 'test_image.png')

      return cy.form_request('http://localhost:3000/graphql', body)
    })
  })

Cypress.Commands.add('createEmptyManuscript', () => {
  const createManuscriptMutation = `
    mutation CreateManuscript {
      createManuscript
    }
  `

  makeRequest({
    query: createManuscriptMutation,
  })
})

Cypress.Commands.add('getCurrentUser', () => {
  const currentUserQuery = `
    query CurrentUser {
      currentUser {
        admin
        id
        teams {
          id
        }
        username
      }
    }
  `

  makeRequest({
    query: currentUserQuery,
  })
})

Cypress.Commands.add('createInitialSubmission', () => {
  const data = manuscriptData
  const fixture = 'files/submission_image.png'
  const fileName = 'test_image.png'

  uploadProgrammatically(fixture, fileName).then(res => {
    const fileUrl = res.response.body.data.upload.url

    data.image = {
      name: fileName,
      url: fileUrl,
    }

    all(cy.createEmptyManuscript, cy.createEmptyManuscript).then(responses => {
      const ids = responses.map(r => r.body.data.createManuscript)

      const fns = ids.map(id => {
        const d = clone(data)
        d.id = id
        const fn = () =>
          makeRequest({
            query: submitManuscriptMutation,
            variables: { data: d },
          })

        return fn
      })

      all(...fns)
    })
  })
})

Cypress.Commands.add('createManuscriptWithGeneExpressionDatatype', () => {
  const data = manuscriptData
  const fixture = 'files/submission_image.png'
  const fileName = 'test_image.png'

  uploadProgrammatically(fixture, fileName).then(res => {
    cy.createEmptyManuscript().then(res2 => {
      const id = res2.body.data.createManuscript
      const fileUrl = res.response.body.data.upload.url

      data.id = id
      data.image = {
        name: fileName,
        url: fileUrl,
      }

      makeRequest({
        query: submitManuscriptMutation,
        variables: { data },
      })

      cy.login('editor')

      const newStatus = clone(data.status)
      newStatus.submission.datatypeSelected = true

      makeRequest({
        query: submitManuscriptMutation,
        variables: {
          data: {
            id,
            dataType: 'geneExpression',
            status: newStatus,
          },
        },
      })
    })
  })
})

Cypress.Commands.add('form_request', (url, formData) =>
  cy
    .server()
    .route('POST', url)
    .as('formRequest')
    .window()
    .then(win => {
      const xhr = new win.XMLHttpRequest()
      xhr.open('POST', url)
      xhr.send(formData)
    })
    .wait('@formRequest'),
)

const all = (...fns) => {
  const results = []

  fns.reduce((prev, fn) => {
    fn().then(result => results.push(result))
    return results
  }, results)

  return cy.wrap(results)
}

const makeRequest = body => {
  const token = window.localStorage.getItem('token')

  return cy.request({
    method: 'POST',
    url: '/graphql',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      authorization: `Bearer ${token}`,
    },
    body,
  })
}
