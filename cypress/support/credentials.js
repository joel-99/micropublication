/* eslint-disable sort-keys */

module.exports = {
  admin: {
    username: 'admin',
    password: 'password',
  },
  author: {
    username: 'author',
    password: 'password',
  },
  editor: {
    username: 'editor',
    password: 'password',
  },
  scienceOfficer: {
    username: 'scienceOfficer',
    password: 'password',
  },
  reviewer: {
    username: 'reviewer',
    password: 'password',
  },
}
