extend type Mutation {
  saveSubmissionForm(input: SubmissionFormInput!): ID!
  setSOApproval(manuscriptVersionId: ID!, approval: Boolean!): Boolean!
  submitManuscript(input: SubmissionFormInput!): ID!
  submitDecision(manuscriptVersionId: ID!, input: SubmitDecisionInput!): ID!
}

type ManuscriptVersion {
  id: ID!
  created: String
  updated: String
  active: Boolean
  submitted: Boolean
  isApprovedByScienceOfficer: Boolean

  acknowledgements: String
  authors: [Author]
  comments: String
  disclaimer: Boolean
  funding: String
  geneExpression: GeneExpression
  image: File
  imageCaption: String
  laboratory: WBItem
  methods: String
  reagents: String
  patternDescription: String
  references: [Reference]
  suggestedReviewer: WBPerson
  title: String

  decision: String
  decisionLetter: String

  reviews(currentUserOnly: Boolean): [Review!]
  teams: [Team!]
}

input SubmitDecisionInput {
  decision: String!
  decisionLetter: String!
}

input SubmissionFormInput {
  acknowledgements: String
  authors: [AuthorInput]
  comments: String
  disclaimer: Boolean
  funding: String
  geneExpression: GeneExpressionInput
  id: ID!
  image: FileInput
  imageCaption: String
  laboratory: WBItemInput
  methods: String
  reagents: String
  patternDescription: String
  references: [ReferenceInput]
  suggestedReviewer: WBPersonInput
  title: String
}

type Author {
  affiliations: [String]
  credit: [String]
  email: String
  id: ID
  firstName: String
  lastName: String
  submittingAuthor: Boolean
  correspondingAuthor: Boolean
  equalContribution: Boolean
  WBId: String
}

input AuthorInput {
  affiliations: [String]
  credit: [String]
  email: String
  id: ID
  firstName: String
  lastName: String
  submittingAuthor: Boolean
  correspondingAuthor: Boolean
  equalContribution: Boolean
  WBId: String
}

type Reference {
  reference: String
  pubmedId: String
  doi: String
}

input ReferenceInput {
  reference: String
  pubmedId: String
  doi: String
}

type File {
  name: String
  url: String
  size: String
  type: String
}

input FileInput {
  name: String
  url: String
  size: String
  type: String
}

type GeneExpression {
  antibodyUsed: String
  backboneVector: WBItem
  coinjected: String
  constructComments: String
  constructionDetails: String
  detectionMethod: String
  dnaSequence: [WBItem]
  expressionPattern: WBItem
  fusionType: WBItem
  genotype: String
  injectionConcentration: String
  inSituDetails: String
  integratedBy: WBItem
  observeExpression: ObserveExpression
  reporter: WBItem
  species: WBItem
  strain: String
  transgeneName: String
  transgeneUsed: [WBItem]
  utr: WBItem
  variation: WBItem
}

input GeneExpressionInput {
  antibodyUsed: String
  backboneVector: WBItemInput
  coinjected: String
  constructComments: String
  constructionDetails: String
  detectionMethod: String
  dnaSequence: [WBItemInput]
  expressionPattern: WBItemInput
  fusionType: WBItemInput
  genotype: String
  injectionConcentration: String
  inSituDetails: String
  integratedBy: WBItemInput
  observeExpression: ObserveExpressionInput
  reporter: WBItemInput
  species: WBItemInput
  strain: String
  transgeneName: String
  transgeneUsed: [WBItemInput]
  utr: WBItemInput
  variation: WBItemInput
}

type ObserveExpression {
  certainly: [ObserveExpressionCertainlyEntry]
  not: [ObserveExpressionNotEntry]
  partially: [ObserveExpressionPartiallyEntry]
  possibly: [ObserveExpressionPossiblyEntry]
}

type ObserveExpressionCertainlyEntry {
  certainly: WBItem
  during: WBItem
  id: ID
  subcellularLocalization: WBItem
}

type ObserveExpressionPartiallyEntry {
  partially: WBItem
  during: WBItem
  id: ID
  subcellularLocalization: WBItem
}

type ObserveExpressionPossiblyEntry {
  possibly: WBItem
  during: WBItem
  id: ID
  subcellularLocalization: WBItem
}

type ObserveExpressionNotEntry {
  not: WBItem
  during: WBItem
  id: ID
  subcellularLocalization: WBItem
}

input ObserveExpressionInput {
  certainly: [ObserveExpressionCertainlyEntryInput]
  not: [ObserveExpressionNotEntryInput]
  partially: [ObserveExpressionPartiallyEntryInput]
  possibly: [ObserveExpressionPossiblyEntryInput]
}

input ObserveExpressionCertainlyEntryInput {
  certainly: WBItemInput
  during: WBItemInput
  id: ID
  subcellularLocalization: WBItemInput
}

input ObserveExpressionPartiallyEntryInput {
  during: WBItemInput
  id: ID
  partially: WBItemInput
  subcellularLocalization: WBItemInput
}

input ObserveExpressionPossiblyEntryInput {
  during: WBItemInput
  id: ID
  possibly: WBItemInput
  subcellularLocalization: WBItemInput
}

input ObserveExpressionNotEntryInput {
  during: WBItemInput
  id: ID
  not: WBItemInput
  subcellularLocalization: WBItemInput
}

type WBItem {
  name: String
  type: String
  WBId: String
}

input WBItemInput {
  name: String
  type: String
  WBId: String
}

type WBPerson {
  name: String
  WBId: String
}

input WBPersonInput {
  name: String
  WBId: String
}
