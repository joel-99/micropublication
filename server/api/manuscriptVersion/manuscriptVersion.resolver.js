const { transaction } = require('objection')
const clone = require('lodash/clone')

const { Manuscript, ManuscriptVersion } = require('@pubsweet/models')
const logger = require('@pubsweet/logger')

const { REVIEWER_STATUSES } = require('../constants')
const { notify } = require('../../services')

const manuscriptVersions = async (
  manuscript,
  { activeOnly, invitedToReviewOnly, last },
  ctx,
) => {
  const { user: userId } = ctx

  let query = ManuscriptVersion.query().orderBy('created', 'asc')

  const where = {
    manuscriptId: manuscript.id,
  }

  if (activeOnly) where.active = true
  if (last) query.limit(last)

  if (invitedToReviewOnly) {
    query = query
      .leftJoin('teams', 'teams.object_id', 'manuscript_versions.id')
      .leftJoin('team_members', 'team_members.team_id', 'teams.id')
      .whereIn('team_members.status', [
        REVIEWER_STATUSES.accepted,
        REVIEWER_STATUSES.rejected,
        REVIEWER_STATUSES.invited,
      ])

    where['teams.role'] = 'reviewer'
    where['team_members.userId'] = userId
  }

  const versions = await query.where(where)

  // TO DO -- this needs to change when we have more than one data type
  const restructured = versions.map(version => {
    const v = clone(version)
    v.geneExpression = version.dataTypeFormData && version.dataTypeFormData.data
    return v
  })

  return restructured
}

const saveSubmissionForm = async (_, { input }, ctx) => {
  const data = clone(input)

  const versionId = data.id
  delete data.id

  if (data.geneExpression) {
    data.dataTypeFormData = {
      data: data.geneExpression,
      type: 'geneExpression',
    }
    delete data.geneExpression
  }

  return ManuscriptVersion.query()
    .findById(versionId)
    .patch(data)
}

const setSOApproval = async (_, { manuscriptVersionId, approval }, ctx) => {
  try {
    await ManuscriptVersion.query()
      .patch({ isApprovedByScienceOfficer: approval })
      .findById(manuscriptVersionId)
      .throwIfNotFound()

    notify('scienceOfficerApprovalStatusChange', {
      versionId: manuscriptVersionId,
    })

    return approval
  } catch (e) {
    logger.error('Set SO approval: Update failed!')
    throw new Error(e)
  }
}

const submitDecision = async (_, { manuscriptVersionId, input }, ctx) => {
  const { user: userId } = ctx
  const { decision, decisionLetter } = input
  const accept = decision === 'accept'
  const reject = decision === 'reject'
  const revise = decision === 'revise'
  let version

  try {
    await transaction(ManuscriptVersion.knex(), async trx => {
      version = await ManuscriptVersion.query(trx).findById(manuscriptVersionId)

      if (version.decision)
        throw new Error('Submit decision: Version already has a decision!')

      await version.$query(trx).patch({
        decision,
        decisionLetter,
      })

      if (revise) await version.makeNewVersion(trx)
    })

    const notifyContext = { userId, version }
    if (accept) notify('articleAccepted', notifyContext)
    if (reject) notify('articleRejected', notifyContext)
    if (revise) notify('articleRevision', notifyContext)

    return version.id
  } catch (e) {
    logger.error('Submit decision: Transaction failed! Rolling back...')
    throw new Error(e)
  }
}

const submitManuscript = async (_, { input }, ctx) => {
  const { user: userId } = ctx
  const { id } = input
  const data = clone(input)

  const version = await ManuscriptVersion.query().findById(id)
  const manuscript = await Manuscript.query().findById(version.manuscriptId)
  const numberOfVersions = await ManuscriptVersion.query()
    .where({
      manuscriptId: manuscript.id,
    })
    .resultSize()

  const { isInitiallySubmitted } = manuscript
  let updatedVersion

  try {
    await transaction(ManuscriptVersion.knex(), async trx => {
      if (version.submitted)
        throw new Error(
          'Submit manuscript: This version has already been submitted',
        )

      if (data.geneExpression) {
        data.dataTypeFormData = {
          data: data.geneExpression,
          type: 'geneExpression',
        }
        delete data.geneExpression
      }

      if (!isInitiallySubmitted) {
        await manuscript.$query(trx).patch({ isInitiallySubmitted: true })
      } else {
        data.submitted = true
      }

      updatedVersion = await version.$query(trx).patchAndFetch(data)

      // Send email
      const notificationContext = { userId, version: updatedVersion }

      if (!isInitiallySubmitted)
        notify('initialSubmission', notificationContext)

      if (isInitiallySubmitted && numberOfVersions === 1)
        notify('fullSubmission', notificationContext)

      if (isInitiallySubmitted && numberOfVersions > 1)
        notify('revisionSubmitted', notificationContext)
    })

    return updatedVersion.id
  } catch (e) {
    logger.error('Submit manuscript: Transaction failed! Rolling back...')
    throw new Error(e)
  }
}

module.exports = {
  manuscriptVersions,
  saveSubmissionForm,
  setSOApproval,
  submitDecision,
  submitManuscript,
}
