const REVIEWER_STATUSES = {
  accepted: 'acceptedInvitation',
  added: 'notInvited',
  invited: 'invited',
  rejected: 'rejectedInvitation',
}

module.exports = { REVIEWER_STATUSES }
