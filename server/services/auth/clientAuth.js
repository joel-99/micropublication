const { TeamMember, User } = require('@pubsweet/models')
const uniq = require('lodash/uniq')

const { REVIEWER_STATUSES } = require('../../api/constants')

/* 
  HELPERS
*/

const manuscriptFromMemberQuery = () =>
  TeamMember.query()
    .select(
      'team_members.*',
      'teams.role',
      'manuscripts.id as manuscriptId',
      'manuscript_versions.id as manuscriptVersionId',
    )
    .leftJoin('teams', 'team_members.team_id', 'teams.id')
    .leftJoin(
      'manuscript_versions',
      'teams.object_id',
      'manuscript_versions.id',
    )
    .leftJoin(
      'manuscripts',
      'manuscript_versions.manuscript_id',
      'manuscripts.id',
    )

const teamFromMemberQuery = () =>
  TeamMember.query()
    .select('team_members.*', 'teams.global', 'teams.role')
    .leftJoin('teams', 'team_members.team_id', 'teams.id')

/* 
  END HELPERS
*/

const createClientAuth = async userId => {
  const user = await User.query().findById(userId)

  /* 
    Is user member of any global team
  */
  const globalMembership = await teamFromMemberQuery().where({
    global: true,
    userId,
  })

  const isGlobal = globalMembership.length > 0 || user.admin
  const isGlobalEditor = !!globalMembership.find(t => t.role === 'editors')
  const isGlobalScienceOfficer = !!globalMembership.find(
    t => t.role === 'scienceOfficers',
  )

  /* 
    Manuscripts that the user is an author of
  */
  const authorManuscripts = await manuscriptFromMemberQuery().where({
    role: 'author',
    userId,
  })

  const isAuthor = authorManuscripts.map(m => m.manuscriptId)

  /*
    Manuscripts (not versions) that the user has accepted an invitation
    to review
  */
  const acceptedReviewInvitationManuscripts = await manuscriptFromMemberQuery().where(
    {
      role: 'reviewer',
      'team_members.status': REVIEWER_STATUSES.accepted,
      userId,
    },
  )

  const isAcceptedReviewerForManuscript = uniq(
    acceptedReviewInvitationManuscripts.map(m => m.manuscriptId),
  )

  const isAcceptedReviewerForVersion = acceptedReviewInvitationManuscripts.map(
    m => m.manuscriptVersionId,
  )

  return {
    isAcceptedReviewerForManuscript,
    isAcceptedReviewerForVersion,
    isAuthor,
    isGlobal,
    isGlobalEditor,
    isGlobalScienceOfficer,
  }
}

module.exports = createClientAuth
