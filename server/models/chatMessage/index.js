const model = require('./chatMessage.model')

module.exports = {
  model,
  modelName: 'ChatMessage',
}
