const model = require('./manuscriptVersion.model')

module.exports = {
  model,
  modelName: 'ManuscriptVersion',
}
