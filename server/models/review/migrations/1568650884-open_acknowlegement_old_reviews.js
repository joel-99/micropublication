const { Review } = require('@pubsweet/models')

exports.up = async knex => {
  const reviews = await Review.query()

  await Promise.all(
    reviews.map(async review => {
      if (review.status.submitted) {
        await review.$query().patch({
          openAcknowledgement: false,
        })
      }
    }),
  )
}
