const BaseModel = require('@pubsweet/base-model')
const { Team } = require('@pubsweet/models')

class Review extends BaseModel {
  static get tableName() {
    return 'reviews'
  }

  constructor(properties) {
    super(properties)
    this.type = 'review'
  }

  static get schema() {
    return {
      properties: {
        articleVersionId: { type: 'string', format: 'uuid' },
        status: { type: 'object' },
        events: { type: 'object' },
        content: { type: 'string' },
        reviewerId: { type: 'string', format: 'uuid' },
        recommendation: { type: 'string' },
        openAcknowledgement: { type: 'boolean' },
      },
    }
  }

  async $beforeDelete() {
    await Team.deleteAssociated(this.data.type, this.id)
  }
}

Review.type = 'review'
module.exports = Review
