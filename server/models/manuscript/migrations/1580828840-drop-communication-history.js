const logger = require('@pubsweet/logger')

exports.up = knex => {
  try {
    return (
      knex.schema
        .hasColumn('manuscripts', 'communication_history')
        /* eslint-disable-next-line consistent-return */
        .then(exists => {
          if (exists) {
            return knex.schema.table('manuscripts', table => {
              table.dropColumn('communication_history')
            })
          }
        })
    )
  } catch (e) {
    logger.error('Manuscript: Drop communication history: Migration failed!')
    throw new Error(e)
  }
}
