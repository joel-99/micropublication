const PubsweetTeam = require('@pubsweet/model-team/src/team')

const { booleanDefaultFalse } = require('../_helpers/types')

class Team extends PubsweetTeam {
  static get schema() {
    return {
      type: 'object',
      required: ['role'],
      properties: {
        global: booleanDefaultFalse,
      },
    }
  }
}

module.exports = Team
