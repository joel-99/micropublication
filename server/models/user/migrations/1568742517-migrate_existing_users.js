const pick = require('lodash/pick')
const { transaction } = require('objection')

const { Identity, User } = require('@pubsweet/models')
const logger = require('@pubsweet/logger')

exports.up = async knex => {
  let userEntities = []
  try {
    userEntities = await knex('entities').where(
      knex.raw(`data ->> 'type' = 'user'`),
    )
  } catch (err) {
    logger.error(
      'Migrate existing users: Failed to get user entities. Most likely "entities" table does not exist',
      err,
    )
  }

  await Promise.all(
    userEntities.map(async user => {
      const userData = pick(user.data, [
        'admin',
        'passwordHash',
        'passwordResetToken',
        'passwordResetTimestamp',
        'username',
      ])

      userData.id = user.id
      userData.passwordResetTimestamp = new Date(
        userData.passwordResetTimestamp,
      )

      try {
        await transaction(User.knex(), async trx => {
          const userExists = await User.query(trx).findOne({
            id: user.id,
            username: user.data.username,
          })

          if (!userExists) await User.query(trx).insert(userData)

          const identityExists = await Identity.query(trx).findOne({
            email: user.data.email,
            userId: user.id,
          })

          if (!identityExists)
            await Identity.query(trx).insert({
              email: user.data.email,
              isConfirmed: true,
              isDefault: true,
              userId: user.id,
            })
        })
      } catch (e) {
        logger.error('Migrate existing users: Failed! Rolling back...')
        throw new Error(e)
      }
    }),
  )
}
