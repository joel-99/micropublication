import React from 'react'
import { Ribbon } from '../src'

export const Default = () => <Ribbon message="Nothing" />

export default { title: 'Ribbon' }
