import React from 'react'
import styled from 'styled-components'

import { Accordion } from '../src'

const Content = styled.div`
  background: green;
  height: 100px;
  width: 100%;
`

export const withContent = () => (
  <Accordion label="do this">
    <Content />
  </Accordion>
)

export const startExpanded = () => (
  <Accordion label="do this" startExpanded>
    <Content />
  </Accordion>
)

export default { title: 'Accordion' }
