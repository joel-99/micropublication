import React from 'react'
import { RibbonFeedback } from '../src'

export const Success = () => (
  <RibbonFeedback keepSpaceOccupied={false} successMessage="Success!">
    {notify => <button onClick={() => notify(true)}>Click me!</button>}
  </RibbonFeedback>
)

export const Error = () => (
  <RibbonFeedback keepSpaceOccupied={false}>
    {notify => (
      <button onClick={() => notify(false, 'Oh no!')}>Click me!</button>
    )}
  </RibbonFeedback>
)

export default { title: 'RibbonFeedback' }
