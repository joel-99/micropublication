/* eslint-disable sort-keys */

const path = require('path')

const webpackConfig = require('./webpack.js')

module.exports = {
  sections: [
    {
      name: 'Basics',
      components: [
        '../app/ui/src/colors/*.js',
        '../app/ui/src/typography/*.js',
      ],
    },
    {},
  ],
  styleguideComponents: {
    Wrapper: path.join(__dirname, 'components/Wrapper'),
  },
  webpackConfig,
}
