/* eslint-disable sort-keys */
const path = require('path')

const resolve = (type, entry) => {
  if (typeof entry === 'string') {
    return require.resolve(`@babel/${type}-${entry}`)
  }
  return [require.resolve(`@babel/${type}-${entry[0]}`), entry[1]]
}

const resolvePreset = entry => resolve('preset', entry)
const resolvePlugin = entry => resolve('plugin', entry)

const include = [
  __dirname,
  path.join(__dirname, '..', 'app'),
  path.join(__dirname, '..', 'node_modules', '@pubsweet', 'ui', 'src'),
]

module.exports = {
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        include,
        query: {
          presets: [['env', { modules: false }], 'react'].map(resolvePreset),
          plugins: [
            require.resolve('react-hot-loader/babel'),
            resolvePlugin(['proposal-decorators', { legacy: true }]),
            resolvePlugin('proposal-class-properties'),
          ],
        },
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.woff|\.woff2|\.svg|\.eot|\.ttf/,
        loader: [
          {
            loader: 'url-loader',
            options: {
              prefix: 'font',
              limit: 1000,
            },
          },
        ],
      },
    ],
  },
}
