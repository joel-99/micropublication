/* eslint-disable import/no-extraneous-dependencies, react/prop-types */

import React from 'react'
import { ThemeProvider } from 'styled-components'

import theme from '@pubsweet/coko-theme'

const Wrapper = ({ children }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
)

export default Wrapper
