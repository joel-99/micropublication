const path = require('path')

module.exports = async ({ config }) => {
  if (config.name !== 'manager') {
    const jsLoader = config.module.rules.find(
      rule => rule.use[0].loader === 'babel-loader',
    )

    const UILocation = path.join(__dirname, '..', 'ui')

    const pubsweetUILocation = path.join(
      __dirname,
      '..',
      'node_modules',
      '@pubsweet',
      'ui',
    )

    const xpubEditLocation = path.join(
      __dirname,
      '../',
      'node_modules',
      'xpub-edit',
    )

    const formElementsLocation = path.join(
      __dirname,
      '..',
      'app',
      'components',
      'formElements',
    )

    const formsLocation = path.join(
      __dirname,
      '..',
      'app',
      'components',
      'form',
    )

    const oldUILocation = path.join(__dirname, '..', 'app', 'components', 'ui')

    jsLoader.include = [
      UILocation,
      pubsweetUILocation,
      formElementsLocation,
      xpubEditLocation,
      formsLocation,
      oldUILocation,
    ]
    jsLoader.exclude = []
  }

  return config
}
