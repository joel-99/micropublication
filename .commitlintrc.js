module.exports = {
  extends: ['@commitlint/config-conventional'],
}

/* 
  If you add custom types, add this entry to the exported object
*/
// rules: {
//   'type-enum': [
//     2,
//     'always',
//     [
//       'build',
//       'ci',
//       'chore',
//       // 'content',
//       'docs',
//       'feat',
//       'fix',
//       'perf',
//       'refactor',
//       'revert',
//       'style',
//       'test',
//       // 'tweak',
//     ],
//   ],
// },
